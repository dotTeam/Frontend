/**
 * Created by Chris on 24-Oct-15.
 */
var constant = require('./gulp.constants.js')();

module.exports = function () {
    return {
        "path": {
            "concat": {
                "js": "src.min.js",
                "style": "style.min.css",
                "html": "template.js",
                "libs": "libs.js",
                "angular": "angular.js"
            },
            "angular": {
                "src": [
                    "node_modules/angular/angular.min.js",
                    "node_modules/angular-ui-router/build/angular-ui-router.min.js",
                    "node_modules/angular-jwt/dist/angular-jwt.min.js",
                    "node_modules/ngstorage/ngStorage.min.js",
                    "node_modules/angular-sanitize/angular-sanitize.min.js",
                    "assets/libs/angular-resource.min.jss",
                    "node_modules/angular-animate/angular-animate.min.js",
                    "node_modules/angular-aria/angular-aria.min.js",
                    "node_modules/angular-material/angular-material.min.js",
                    "node_modules/socket.io-client/socket.io.js",
                    "node_modules/angular-ui-bootstrap/dist/ui-bootstrap.js",
                    "node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js"
                ]
            },
            "libs": {
                //in libs we put the javascript and the css that we want to use
                "src": [
                    "assets/libs/jquery-2.1.4.min.js",
                    //"assets/libs/trumbowyg.min.js",
                    "assets/**/*.js",
                    "node_modules/moment/min/moment.min.js",
                    "node_modules/fullcalendar/dist/fullcalendar.min.js"

                ],
                "dest": "public/build"
            }
            ,
            "html": {
                "src": "src/**/*.html",
                "dest": "public/build"
            }
            ,
            "js": {
                "src": ["src/**/*.js",
                    "!src/**/*.test.js",
                    "!src/common/constants.example.js"
                ],
                "dest": "public/build/"
            }
            ,
            "styles": {
                //poate ar trebui sa adaug in src si assets/**/*.css care sau fisierele
                //care nu sunt incluse in style.scss dar le folosesc
                "src": [
                    "assets/scss/style.scss",
                    //supravegheaza fisierele .css care nu sunt include in fisierul de deasupra
                    "assets/scss/*.css"
                ],
                "watch": "assets/**/*",
                "dest": "public/build/"
            }
            ,
            "images": {
                "src": "assets/img/**",
                "dest": "public/build/img"
            }
            ,
            "fonts": {
                "src": "assets/fonts/*",
                "dest": "public/build/fonts"
            }
        },
        "livereload": {
            "proxyServer": constant.proxy,
            "path": "public/",
            "port": 3601,
            "flags": "--quiet"
        }
    }
};