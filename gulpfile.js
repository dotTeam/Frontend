// /**
//  * Created by Chris on 22-Oct-15.
//  */
var gulp = require('gulp'),
        concat = require('gulp-concat'),
        uglify = require('gulp-uglify'),
        prefix = require('gulp-autoprefixer'),
        imagemin = require('gulp-imagemin'),
        config = require('./gulp.config.js')(),
        shell = require('gulp-shell'),
        sass = require('gulp-sass'),
        wrap = require("gulp-wrap"),
        templateCache = require('gulp-angular-templatecache'),
        browserSync = require('browser-sync'),
        watch = require('gulp-watch');

//error log function:
function errorLog(error) {
    console.error(error.message);
}

//Scripts task:
//Uglifies
gulp.task('compress-js', function () {
    gulp.src(config.path.js.src)
            .pipe(wrap('(function(){\n"use strict";\n<%= contents %>\n})();'))
            .pipe(concat(config.path.concat.js))
            .pipe(uglify())
            .on('error', errorLog)
            .pipe(gulp.dest(config.path.js.dest));

    //angular libraries:
    gulp.src(config.path.angular.src)
            .pipe(concat(config.path.concat.angular))
            .on('error', errorLog)
        //.pipe(uglify())
            .pipe(gulp.dest(config.path.libs.dest));

    //3rd libraries(like jquery):
    gulp.src(config.path.libs.src)
            .pipe(concat(config.path.concat.libs))
            .pipe(uglify())
            .on('error', errorLog)
            .pipe(gulp.dest(config.path.libs.dest))

});

//
//Style task:
//Uglifies
//
gulp.task('styles', function () {
    gulp.src(config.path.styles.src)
            .pipe(concat(config.path.concat.style))
            .pipe(sass({
                //to have my sass css compresed:
                outputStyle: 'compressed'
                //outputStyle: 'extended'
            }))
            .on('error', errorLog)
            .pipe(prefix({
                browsers: ['last 2 versions', 'safari 5'],
                cascade: false
            }))
            .pipe(gulp.dest(config.path.styles.dest))
});

//Images task:
//Compress the images
gulp.task('images', function () {
    gulp.src(config.path.images.src)
            .pipe(imagemin())
            .pipe(gulp.dest(config.path.images.dest))
});

// Task fonts
gulp.task('fonts', function () {
    return gulp.src(config.path.fonts.src)
            .pipe(gulp.dest(config.path.fonts.dest));
});

//Watch task:
//Watches JS
gulp.task('watch', function () {
    ////the watchers will work on both shared folder and files with vagrant
    //gulp.watch(config.path.angular.src, ['compress-js']);
    //gulp.watch(config.path.js.src, ['compress-js']);
    //gulp.watch(config.path.styles.watch, ['styles']);
    //gulp.watch(config.path.images.src, ['images']);
    //gulp.watch(config.path.fonts.src, ['fonts']);
    //gulp.watch(config.path.html.src, ['html']);

    //this will work only with npm windows
    watch(config.path.angular.src,function(){
        gulp.run(['compress-js']);
    });

    watch(config.path.js.src,function(){
        gulp.run(['compress-js']);
    });

    watch(config.path.styles.watch,function(){
        gulp.run(['styles']);
    });

    watch(config.path.images.src,function(){
        gulp.run(['images']);
    });

    watch(config.path.fonts.src,function(){
        gulp.run(['fonts']);
    });

    watch(config.path.html.src,function(){
        gulp.run(['html']);
    });

});

//Live reload
//this is deprecated :))
gulp.task('live-server',
        shell.task([
            //aici ar trebui sa alegi in functie de sistemul de operare pe care ruleaza
            //gulp ori sa desparti cu & pentru windows ori ; pentru unix
            //in modul in care am pus o sa mearga la amandoua dar o sa imi apara mici warnings
            "cd " + config.livereload.path + ";" +
            "cd " + config.livereload.path + "&" +
            "cd " + config.livereload.path + "&" +
            "live-server --port=" + config.livereload.port
        ])
);

gulp.task('browser-sync', function () {
    browserSync({
        proxy: config.livereload.proxyServer,
        port: config.livereload.port,
        files: config.livereload.path,
        ghostMode: {
            clicks: true,
            location: true,
            forms: true,
            scroll: true
        },
        watch: false,
        logFileChanges: false,
        open: false
    });
});

//Build task
gulp.task('build', function () {
    //build all source code
    gulp.start('compress-js', 'styles', 'images', 'html', 'fonts');
});

// Gulp task for creating template cache
gulp.task('templatecache', function () {
    log('Creating an AngularJS $templateCache');

    return gulp
            .src(config.path.html.src)
            .pipe($.minifyHtml({empty: true}))
            .pipe($.angularTemplatecache(
                    config.templateCache.file,
                    config.templateCache.options
            ))
            .pipe(gulp.dest(config.path.html.dest));
});

gulp.task('html', function () {
    //we can my a template.js file for each component if we want
    //we just create another command just like the one below but we change the config.path
    //variables for example we could create common.tpl.js and core tpl.js but for now
    //we only have template.js
    gulp.src(config.path.html.src)
            .pipe(templateCache(config.path.concat.html, {module: 'template', standalone: true, root: 'src'}))
            .pipe(gulp.dest(config.path.html.dest));
});

////Default Task
gulp.task('default', ['compress-js', 'fonts', 'html', 'styles', 'browser-sync', 'watch']);
