# Frontend for licenta

Angular seed project with gulp and npm packages

## config

To configure you environmental variable config gulp.config.js

## Tools needed:

1. [nodejs](https://nodejs.org/en/)
 

## Install

you must run it with windows administrator rights

```
$ npm install
```
- this will install dependencies from package.json

```
$ npm run ng-install
```
- if you want to install just the production packages type the command above

```
$ cp src/common/constants.example.js src/common/constants.js
```


```
$ cp gulp.constants.example.js gulp.constants.js
```

### Gulp commands ###

```
$ npm run dev
```
- start the development envelopment
- this will start a livereload page on port 3600 you can modify this in gulp.config.js

```
$ npm run build
```
- compile the scripts(javascript,scss,html) 
- concat, minifies and put them in the build folder
- compress the images

```
$ npm run live-server
```
- start a live-server on port 3600