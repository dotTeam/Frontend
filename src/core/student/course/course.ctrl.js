angular
    .module("app.core.student.course")
    .controller("course", course);

course.$inject = [
    '$state',
    'moduleCrud',
    '$stateParams',
    'courseServ'
];

function course($state, moduleCrud, $stateParams, courseServ) {
    var vm = this;

    vm.name = 'loading name';
    vm.description = 'loading description';
    vm.course_id = null;
    vm.unsubscribe = unsubscribe;

    vm.hide = false;
    vm.show = show;
    vm.stop = stop;

    var ModuleCrud;

    activate();

    //////////////////////////////////////////

    function stop(){
        vm.hide = false;
    }
    function show(){
        vm.hide = true;
    }
    function activate() {
        vm.course_id = $stateParams.course_id;

        ModuleCrud = new moduleCrud.crud();

        courseServ.get_course_by_id($stateParams.course_id,
            function (data) {
                vm.course = data;

                if (vm.course.id != null) {
                    moduleCrud.getmodules(vm.course.id, function (data) {

                        vm.modules = data.course.module;
                    });
                }
            });
    }

    function unsubscribe(){
        var data = "/" + $stateParams.course_id
            + "/user/" + window.sessionStorage.getItem("user_id");
        courseServ.unsubscribe(data,function(d){
            $state.go("layout.shared.course",{course_id:$stateParams.course_id});
        });
    }
}
