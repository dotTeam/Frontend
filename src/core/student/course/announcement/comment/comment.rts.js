angular
	.module('app.core.student.course.announcement.comment')
	.config(commentRoute);

commentRoute.$inject = [
	'$stateProvider'
]

function commentRoute($stateProvider) {
	$stateProvider
		//insert your layout in place of the word LAYOUT
		.state('layout.student.course.announcement.comment', {
			//rename URL with your url
			url: '/:announcement_id/comments',
			templateUrl: 'src/core/student/course/announcement/comment/comment.tpl.html',
			controller: 'commentStudent as commentCtrl',
		})
}
