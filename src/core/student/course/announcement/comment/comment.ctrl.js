angular
	.module('app.core.student.course.announcement.comment')
	.controller('commentStudent', comment);

comment.$inject = [
	'commentSrv',
	'$stateParams',
	'urls'
]

function comment(commentSrv, $stateParams, urls) {

	var vm = this;
	vm.name = 'comment';
	vm.comments = [];
	
	var crud=null;

	vm.add = add;
	vm.token = null;
	vm.img_url = null;

	activate();

	//////////////////////////////////////////

	function activate() {
		vm.img_url = urls.BASE_API;

		console.log('controller comment initialization');

		vm.token = window.sessionStorage.getItem("token");

		crud = new commentSrv.crud();

		commentSrv.get($stateParams.announcement_id,function(d){
			vm.comments = d.announcement_course.comment_announcement_course;
		});
	}

	function add() {
		var comment = {
			text: vm.text,
			announcement_course_id: $stateParams.announcement_id
		}
		crud.save(comment,function(d){
			vm.comments.push(d.comment_announcement);
			vm.text = null;
		});
	}
}