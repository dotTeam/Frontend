angular
    .module('app.core.student.course.announcement')
    .config(announcementRoute);

announcementRoute.$inject = [
    '$stateProvider'
]

function announcementRoute($stateProvider) {
    $stateProvider
    //insert your layout in place of the word LAYOUT
        .state('layout.student.course.announcement', {
            //rename URL with your url
            url: '/announcement',
            views: {
                "course_role_module@layout.student.course": {

                    templateUrl: 'src/core/student/course/announcement/announcement.tpl.html',
                    controller: 'announcement as announcementCtrl'
                }
            }
        })
}
