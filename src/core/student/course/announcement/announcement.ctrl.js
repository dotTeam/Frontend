angular
	.module('app.core.student.course.announcement')
	.controller('announcementStudent', announcement);

announcement.$inject = [
	'announcementSrv',
	'$stateParams',
	'urls'
]

function announcement(announcementSrv, $stateParams, urls) {

	var vm = this;
	vm.name = 'announcement';
	vm.announcements = [];
	vm.token = null;
	
	var crud=null;

	activate();

	//////////////////////////////////////////

	function activate() {
		console.log('controller announcement initialization');

		vm.img_url = urls.BASE_API;
		vm.token = window.sessionStorage.getItem("token");

		var crt = $stateParams.course_id;

		crud = new announcementSrv.crud();

		announcementSrv.get(crt, function (data) {
				console.log(data);
				vm.announcements = data.course.announcement_course;
			}
		);
	}
}