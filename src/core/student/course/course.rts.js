angular
    .module("app.core.student.course")
    .config(courseRoute)

courseRoute.$inject = [
    '$stateProvider'
]

function courseRoute($stateProvider) {
    $stateProvider
        .state('layout.student.course', {
            url: '/course/:course_id',
            views: {
                "": {
                    templateUrl: "src/core/student/course/course.tpl.html",
                    controller: "course as courseCtrl"
                },
                "course_role_module@layout.student.course": {
                    templateUrl: "src/core/student/course/module/module.tpl.html",
                    controller: "moduleStudent as moduleCtrl"
                }
            }

        })
        .state('layout.student.course.module', {
            views: {
                "course_role_module@layout.student.course": {
                    templateUrl: "src/core/student/course/module/module.tpl.html",
                    controller: "moduleStudent as moduleCtrl"
                }
            }
        })
}
