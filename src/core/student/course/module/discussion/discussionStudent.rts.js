angular
	.module('app.core.student.course.module.discussion')
	.config(discussionStudentRoute);

discussionStudentRoute.$inject = [
	'$stateProvider'
]

function discussionStudentRoute($stateProvider) {
	$stateProvider
		.state('layout.student.home.discussion', {
			url: '/discussion',
			templateUrl: 'src/core/student/course/module/discussion/discussionStudent.tpl.html',
			controller: 'discussionStudent as discussionCtrl'
		})
}
