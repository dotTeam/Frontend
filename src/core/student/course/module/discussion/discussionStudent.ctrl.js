angular
	.module('app.core.student.course.module.discussion')
	.controller('discussionStudent', discussionStudent);

discussionStudent.$inject = [
	'discussionSrv',
	'$stateParams',
	'urls'
]

function discussionStudent(discussionSrv, $stateParams, urls) {

	var vm = this;
	vm.name = 'discussion';
	vm.discussions = [];
	vm.comment = null;

	vm.subject = null;
	vm.description = null;
	vm.category = "important";

	vm.addComment = addComment;
	vm.addDiscussion = addDiscussion;

	vm.removeDiscussion = removeDiscussion;
	vm.removeComment = removeComment;

	activate();

	//////////////////////////////////////////

	function activate() {
		vm.my_user_id = window.sessionStorage.getItem('user_id');
		vm.img_url = urls.BASE_API;
		vm.token = window.sessionStorage.getItem('token');
		vm.my_name = window.sessionStorage.getItem('user_name');

		discussionSrv.get($stateParams.module_id, function (data) {
			var discussions = data.module.discussion;
			var rez = [];
			for (var i in discussions) {

				discussionSrv.get_comments(discussions[i].id,i, function (d,index_disscusion) {
					d.discussion['user'] = {
						'name':discussions[index_disscusion].user.name
					};
					rez.push(d.discussion);
					rez.sort(function (a, b) {
						//le afiseaza random pentru ca in seeduri
						//datele sunt egale
						var a_local = Date.parse(a.created_at.replace(' ', 'T'));
						var b_local = Date.parse(b.created_at.replace(' ', 'T'));
						return a_local - b_local;
					})
					vm.discussions = rez;
				});
			}
		});
	}

	function addComment(text, discussion_id) {
		var comment = {
			text: text,
			discussion_id: discussion_id
		}
		discussionSrv.post_comment(comment, function (d) {
			for (var i in vm.discussions) {
				if (vm.discussions[i].id == discussion_id) {
					d.comment['user'] = {
						'name':vm.my_name
					};
					if(vm.discussions[i].comment == null)
						vm.discussions[i].comment = [];
					vm.discussions[i].comment.push(d.comment);
				}
			}
		});
	}

	function addDiscussion() {
		var discussion = {
			subject: vm.subject,
			description: vm.description,
			category: vm.category,
			module_id: $stateParams.module_id,
		}
		discussionSrv.post_discussion(discussion, function (d) {
			console.log(d);
			d.discussion['user'] = {
				'name':vm.my_name
			};
			vm.discussions.push(d.discussion);

			vm.subject = null;
			vm.description = null;
			//TODO INTREABA PE BACKEND DE UNDE SA I-A CATEGORI PENTRU DISCUTII
			vm.category = "important";
		});
	}

	function removeDiscussion(id){
		discussionSrv.remove_discussion(id,function(d){
			for(var i in vm.discussions){
				if(vm.discussions[i].id == id) {
					vm.discussions.splice(i, 1);
				}
			}
		})
	}

	function removeComment(comment_id,discussion_id){
		discussionSrv.remove_comment(comment_id,function(d){
			for(var i in vm.discussions){
				if(vm.discussions[i].id == discussion_id) {
					for(var j in vm.discussions[i].comment){
						if(vm.discussions[i].comment[j].id == comment_id){
							vm.discussions[i].comment.splice(j,1);
						}
					}
				}
			}
		})
	}
}