angular
    .module('app.core.student.course.module.test')
    .controller('testStudent', testStudent)


testStudent.$inject = [
    'testSrv',
    '$stateParams',
    'urls'
]

function testStudent(testSrv, $stateParams, urls) {

    var vm = this;
    vm.tests = [];

    vm.img_url = urls.BASE_API;
    vm.token = window.sessionStorage.getItem('token');

    var crud = null;

    activate();

    //////////////////////////////////////////

    function activate() {
        console.log('controller test initialization');

        crud = new testSrv.crud();

        testSrv.get_test($stateParams.module_id, function (data) {
                vm.tests = data.module.test;
            }
        );
    }
}