angular
	.module('app.core.student.course.module.test')
	.config(testRoute);

testRoute.$inject = [
	'$stateProvider'
]

function testRoute($stateProvider) {
	$stateProvider
		//insert your layout in place of the word LAYOUT
		.state('layout.student.home.test', {
			//rename URL with your url
			url: '/test',
			templateUrl: 'src/core/student/course/module/test/test.tpl.html',
			controller: 'testStudent as testCtrl',
		})
}
