angular
    .module('app.core.student.course.module.test.question')
    .controller('questionStudent', questionStudent);

questionStudent.$inject = [
    'questionSrv',
    '$stateParams',
    '$scope'
]

function questionStudent(questionSrv, $stateParams, $scope) {

    var vm = this;
    vm.name = 'question';
    vm.questions = [];
    vm.test_duration = null;

    vm.submit_test = submit_test;
    vm.rez = null;

    var crud = null;
    var my_answers = [];
    var time = null;

    activate();

    //////////////////////////////////////////

    function activate() {
        console.log('controller question initialization');
        crud = new questionSrv.crud();
        questionSrv.get($stateParams.test_id, function (data) {
                console.log(data);
                for (var i in data.test.question) {
                    data.test.question[i].answer = JSON.parse(data.test.question[i].answer);
                    vm.questions.push(data.test.question[i]);

                    var my_answer = {
                        "data": [],
                        "type": null,
                        "question_id": data.test.question[i].answer.question_id
                    }
                    my_answers.push(my_answer);

                }

                setTimeout(listen, 100);

                vm.test_duration = data.test.duration;
                time = setInterval(function () {
                    vm.test_duration--;
                    $scope.$apply();
                    if (vm.test_duration == 0) {
                        submit_test();
                        //console.log("gata cu inima");
                        clearInterval(time);
                    }
                }, 1000);
            }
        );
    }

    function listen() {
        for (var i in vm.questions) {
            listen_answer(vm.questions[i].answer, vm.questions[i].answer.question_id)
        }
    }

    function listen_answer(data, id) {
        if (data.type == 'radio') {
            listen_radio('radio_' + id, id);
        }
        if (data.type == 'checkbox') {
            listen_check('check_' + id, id);
        }
        if (data.type == 'text') {
            listen_text("text_" + id, id);
        }
    }

    function listen_radio(name, id) {
        $('#myForm input').on('change', function () {
            var rez = $('input[name=' + name + ']:checked', '#myForm').val();
            for (var i in my_answers) {
                if (my_answers[i].question_id == id) {
                    my_answers[i].data = [];
                    my_answers[i].data.push({
                        id: rez
                    });
                    break;
                }
            }
        });
    }

    function listen_check(name, id) {
        $('#myForm input').on('change', function () {
            var values = new Array();
            $.each($("input[name='" + name + "']:checked"), function () {
                values.push($(this).val());
                for (var i in my_answers) {
                    if (my_answers[i].question_id == id) {
                        my_answers[i].data = [];
                        for (var j in values) {
                            my_answers[i].data.push({
                                id: values[j]
                            });

                        }
                        break;
                    }
                }
            });
        });
    }

    function listen_text(name, id) {
        $('#myForm input').on('change', function () {
            var values = new Array();
            var ids = [];
            $.each($("input[name=" + name + "]"), function () {
                values.push($(this).val());
                ids.push($(this).attr('id'));
            });
            for (var i in my_answers) {
                if (my_answers[i].question_id == id) {
                    my_answers[i].data = [];
                    for (var j in values) {
                        my_answers[i].data.push({
                            data: values[j],
                            id: "" + ids[j] + ""
                        });
                    }
                    break;
                }
            }
        });
    }

    function submit_test() {
        var str_my_answers = JSON.stringify(my_answers);

        clearInterval(time);
        vm.test_duration = 0;

        questionSrv.submit_answer(str_my_answers,function(data){
            console.log(data.response_test);
            //console.log(my_answers);
            //var correct = 0;
            //var corect = data.response_test;
            //for(var i in my_answers){
            //    if(my_answers[i] == corect[i]){
            //        correct++;
            //    }
            //}
            //console.log(correct);
            //vm.rez = JSON.stringify(str_my_answers.response_test);
            //$scope.$apply();
        })
    }
}