angular
	.module('app.core.student.course.module.test.question')
	.config(questionRoute);

questionRoute.$inject = [
	'$stateProvider'
]

function questionRoute($stateProvider) {
	$stateProvider
		.state('layout.student.home.question', {
			url: '/question/:test_id',
			templateUrl: 'src/core/student/course/module/test/question/question.tpl.html',
			controller: 'questionStudent as questionCtrl'
		})
}
