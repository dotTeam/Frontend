angular
    .module('app.core.student.course.module.home')
    .config(homeStudentRoute);

homeStudentRoute.$inject = [
    '$stateProvider'
]

function homeStudentRoute($stateProvider) {
    $stateProvider.state('layout.student.home', {
        url: '/course/:course_id/module/:module_id',
        templateUrl: 'src/core/student/course/module/home/homeStudent.tpl.html',
        controller: 'homeStudent as homeStudentCtrl',
    })
}
