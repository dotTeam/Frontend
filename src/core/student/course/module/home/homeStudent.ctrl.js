angular
    .module('app.core.student.course.module.home')
    .controller('homeStudent', homeStudent);

homeStudent.$inject = [
    '$stateParams',
    'moduleCrud',
    'pageCrud'
]

function homeStudent($stateParams, moduleCrud, pageCrud) {

    var vm = this;
    vm.name = 'homeStudent';
    vm.homeStudents = [];
    vm.page = null;
    activate();

    //////////////////////////////////////////

    function activate() {
        console.log('controller homeStudent initialization');
        vm.course_id = $stateParams.course_id;
        vm.module_id = $stateParams.module_id;

        moduleCrud.getmodule(vm.course_id, vm.module_id, function (d) {
            vm.module = d;
            vm.started_at = new Date(d.started_at);
        });

        moduleCrud.getmodules(vm.course_id,function(data){
           vm.course_name = data.course.name;
        });

        load_page();
    }

    function load_page() {
        pageCrud.get_page(vm.module_id, function (data) {

            var pages = data.module.page_module;

            //only take the first page

            if (pages[0] != null) {
                vm.page = pages[0];
            }

        })
    }
}