angular
	.module('app.core.student.course.module.homework')
	.config(homeworkRoute);

homeworkRoute.$inject = [
	'$stateProvider'
]

function homeworkRoute($stateProvider) {
	$stateProvider
		//insert your layout in place of the word LAYOUT
		.state('layout.student.home.homework', {
			//rename URL with your url
			url: '/homework',
			templateUrl: 'src/core/student/course/module/homework/homework.tpl.html',
			controller: 'homeworkStudent as homeworkCtrl',
		})
}
