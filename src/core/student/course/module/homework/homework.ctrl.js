angular
    .module('app.core.student.course.module.homework')
    .controller('homeworkStudent', homeworkStudent);

homeworkStudent.$inject = [
    'homeworkSrv',
    '$stateParams',
    'urls',
    '$scope'
]

function homeworkStudent(homeworkSrv, $stateParams, urls, $scope) {

    var vm = this;
    vm.name = 'homework';
    vm.homeworks = [];

    vm.add_text = add_text;
    vm.add_file = add_file;

    vm.upload = null;
    vm.remove_answer = remove_answer;

    var crud = null;

    activate();

    //////////////////////////////////////////

    function activate() {
        vm.img_url = urls.BASE_API;
        vm.token = window.sessionStorage.getItem('token');
        console.log('controller homework initialization');

        crud = new homeworkSrv.crud();

        homeworkSrv.get_module_homeworks($stateParams.module_id, function (data) {
                for (var i in data.module.homework) {
                    homeworkSrv.get_answer_homeworks(data.module.homework[i].id, function (d) {
                        vm.homeworks.push(d.homework);
                    })
                }
                //vm.homeworks = data.module.homework;
            }
        );
    }

    function add_text(text, homework_id) {
        var answer = {
            answer: text,
            homework_id: homework_id
        }
        homeworkSrv.add_answer(answer, function (d) {
            console.log(d);
            for (var i in vm.homeworks) {
                if (vm.homeworks[i].id == homework_id) {
                    vm.homeworks[i].homework_answer.push(d.homework_answer);
                }
            }
        })
    }

    function add_file(homework_id) {
        var upload = {
            upload:vm.upload,
            homework_id:homework_id
        }
        homeworkSrv.add_file(upload, function (d) {
            console.log(d);
            for (var i in vm.homeworks) {
                if (vm.homeworks[i].id == homework_id) {
                    vm.homeworks[i].homework_answer.push(d.homework_answer);
                    $scope.$digest();
                }
            }
        });
    }

    function remove_answer(id) {
        homeworkSrv.remove_answer(id, function (d) {
            console.log(d);
            for (var i in vm.homeworks) {
                for (var j in vm.homeworks[i].homework_answer)
                    if (vm.homeworks[i].homework_answer[j].id == id) {
                        vm.homeworks[i].homework_answer.splice(j, 1);
                    }
            }
        })
    }
}