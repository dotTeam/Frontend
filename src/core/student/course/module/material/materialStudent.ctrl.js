angular
    .module('app.core.student.course.module.material')
    .controller('materialStudent', materialStudent);

materialStudent.$inject = [
    'materialSrv',
    '$stateParams',
    'urls'
]

function materialStudent(materialSrv, $stateParams, urls) {

    var vm = this;
    vm.materials = [];

    vm.img_url = null;
    vm.token = null;
    vm.download = download;

    activate();

    //////////////////////////////////////////

    function activate() {
        console.log('controller materialStudent initialization');

        vm.img_url = urls.BASE_API;
        vm.token = window.sessionStorage.getItem("token");

        materialSrv.module_materials($stateParams.module_id, function (d) {
            vm.materials = d.module.material;
        });
    }

    function download(id){
        //var download_url = urls.BASE_API + "/material/" + id + "?token=" + vm.token;
        console.log(id);
        for(var i in vm.materials){
            if(vm.materials[i].id == id){
                //la download_nr se pune numai odata pe user
                //daca deja l-a download nu mai are rost so nu trebuie neaparat
                //sa increase
                //vm.materials[i].download_nr++;
            }
        }
        //$state.go(download_url);
    }
}