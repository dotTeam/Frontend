angular
	.module('app.core.student.course.module.material')
	.config(materialStudentRoute);

materialStudentRoute.$inject = [
	'$stateProvider'
]

function materialStudentRoute($stateProvider) {
	$stateProvider
		//insert your layout in place of the word LAYOUT
		.state('layout.student.home.material', {
			url: '/material',
			templateUrl: 'src/core/student/course/module/material/materialStudent.tpl.html',
			controller: 'materialStudent as materialStudentCtrl',
		})
}
