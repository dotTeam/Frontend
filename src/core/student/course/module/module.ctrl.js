angular
	.module('app.core.student.course.module')
	.controller('moduleStudent', module);

module.$inject = [
	'$state',
	'moduleCrud',
	'$stateParams',
	'courseServ'
]

function module($state, moduleCrud, $stateParams, courseServ) {

	var vm = this;
	vm.name = 'module';
	vm.modules = [];

	vm.go_to_module = go_to_module;

	var ModuleCrud = null;

	activate();

	//////////////////////////////////////////

	function activate() {
		console.log('controller module initialization');

		ModuleCrud = new moduleCrud.crud();

		courseServ.get_course_by_id($stateParams.course_id,
			function (data) {
				vm.course = data;

				if (vm.course.id != null) {
					moduleCrud.getmodules(vm.course.id, function (data) {

						vm.modules = data.course.module;
					});
				}
			});
	}

	function go_to_module(module){
		$state.go("layout.student.home", {module_id: module.id, course_id: $stateParams.course_id});
	}
}