angular
	.module('app.core.student.course.module', [
		"app.core.student.course.module.home",
		"app.core.student.course.module.material",
		"app.core.student.course.module.discussion",
		"app.core.student.course.module.homework",
		"app.core.student.course.module.test"
	])
