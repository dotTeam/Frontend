angular
    .module('app.core.student')
    .config(studentRoute);

studentRoute.$inject = [
    '$stateProvider'
]

function studentRoute($stateProvider) {

    $stateProvider
        .state('layout.student', {
            url: 'student',
            views: {
                'sidebar-content': {
                    templateUrl: 'src/core/student/sidebar.tpl.html',
                }
                ,
                'main-content': {
                    templateUrl: 'src/core/student/content.tpl.html',
                }
            }
        })
}