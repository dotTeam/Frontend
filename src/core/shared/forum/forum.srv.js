angular
	.module('app.core.shared.forum')
	.factory('forumSrv', forumSrv);

forumSrv.$inject = [
	'$resource',
	'$http',
	'urls',
	'$q'
]

function forumSrv($resource, $http, urls, $q) {
	var service = {
		crud: crud,
		get: get,
		get_all: get_all,
		update:update,
		jump_page:jump_page
	}

	return service;

	////////////////////////////////////////////////////////

	function crud() {
		//replace the word 'WORD' with you resource api endpoint
		return $resource(urls.BASE_API + '/forum/:id', {id: '@_id'}, {
			remove: {
    			method: 'DELETE'
			}
		})
	};
	function get(id, success) {
		//replace the word 'WORD' with you resource api endpoint
		//this function also has an id to get a specific resource
		return $http.get(urls.BASE_API + '/WORD/' + id)
			.success(success)
	}

	function get_all(success_handle) {
		var defer = $q.defer();

		$http.get(urls.BASE_API + '/forum')
			.success(function (d) {
				defer.resolve(success_handle(d));
			});
		return defer.promise;
	}
	function update(data,success){
		$http.put(urls.BASE_API + '/forum' + data)
			.success(success);
	}
	function jump_page(data,success){
		$http.get(data)
			.success(success);
	}
}