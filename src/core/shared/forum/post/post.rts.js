angular
	.module('app.core.shared.forum.post')
	.config(postRoute);

postRoute.$inject = [
	'$stateProvider'
]

function postRoute($stateProvider) {
	$stateProvider
		//insert your layout in place of the word LAYOUT
		.state('layout.shared.forum.post', {
			//rename URL with your url
			url: '/:forum_id/posts',
			templateUrl: 'src/core/shared/forum/post/post.tpl.html',
			controller: 'post as postCtrl',
		})
}
