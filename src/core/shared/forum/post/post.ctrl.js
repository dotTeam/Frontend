angular
    .module('app.core.shared.forum.post')
    .controller('post', post);

post.$inject = [
    'postSrv',
    '$stateParams',
    'urls',
    '$scope'
]

function post(postSrv, $stateParams, urls, $scope) {

    var vm = this;

    vm.name = 'post';
    vm.posts = [];
    vm.post = {
        forum_id: null,
        message: null
    }

    vm.forum = null;
    vm.user_id = null;
    vm.my_user_id = null;

    vm.remove = remove;
    vm.add = add;
    vm.update = update;

    vm.token = null;

    var crud = null;

    activate();

    //////////////////////////////////////////

    function activate() {

        vm.img_url = urls.BASE_API;

        console.log('controller post initialization');

        vm.token = window.sessionStorage.getItem("token");

        vm.my_user_id = window.sessionStorage.getItem("user_id");
        vm.post.forum_id = $stateParams.forum_id;

        crud = new postSrv.crud();

        postSrv.get_all($stateParams.forum_id,function(d){
            for(var l in d.data){
                vm.posts.push(d.data[l]);
            }
        });
        //take the post from the provious view
        //$scope.$watchCollection('forumCtrl.posts', load_posts);
    }

    function load_posts() {
        var posts = $scope.$parent.forumCtrl.posts;
        if (posts != null) {
            for (var i in posts) {
                for (var j in posts[i])
                    if (posts[i][j].forum_id == $stateParams.forum_id) {
                        var ok = 0;
                        for(var x in vm.posts){
                            if(vm.posts[x] == posts[i][j]){
                                ok=1;
                            }
                        }
                        if(ok == 0){
                            vm.posts.push(posts[i][j]);
                        }
                    }
            }
        }
    }

    function remove(id) {
        console.log(id);
        crud.remove({"id": id}, function (d) {
            console.log(d);
            for (var i in vm.posts) {
                if (vm.posts[i].id == id) {
                    vm.posts.splice(i, 1);
                    break;
                }
            }
        });
    }

    function add() {
        vm.post.forum_id = $stateParams.forum_id;

        if (vm.post.forum_id != null) {
            crud.save(vm.post, function (d) {
                console.log("Succesfuly added" + d.post);
                d.post['user'] = {
                    'id': vm.my_user_id,
                    'name': window.sessionStorage.getItem("user_name")
                };
                vm.posts.push(d.post);
            });
        }
    }

    function update(id, message) {
        console.log(id);
        console.log(message);

        var data = "/" + id + "?message=" + message;

        postSrv.update(data, function (d) {
            console.log(d);
        });
    }
}