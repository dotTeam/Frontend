angular
    .module('app.core.shared.forum.post')
    .factory('postSrv', postSrv);

postSrv.$inject = [
    '$resource',
    '$http',
    'urls'
]

function postSrv($resource, $http, urls) {
    var service = {
        crud: crud,
        get_all: get_all,
        update: update,
        get_last_post: get_last_post
    }

    return service;

    ////////////////////////////////////////////////////////

    function crud() {
        return $resource(urls.BASE_API + '/post/:id', {id: '@_id'}, {
            remove: {
                method: 'DELETE'
            }
        })
    };
    function get_all(id, success) {
        return $http.get(urls.BASE_API + '/forum/' + id + '/post?id=!-51&with=user')
            .success(success)
    }

    function update(data, success) {
        $http.put(urls.BASE_API + '/post' + data)
            .success(success);
    }

    function get_last_post(id, index_forum, success) {
        return $http.get(urls.BASE_API + '/forum/' + id + '/post?with=user')
            .success(function (data) {
                success(data, index_forum);
            });
    }
}