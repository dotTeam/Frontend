angular
    .module('app.core.shared.forum')
    .controller('forumEdit', forumEdit);

forumEdit.$inject = [
    'forumSrv',
    '$stateParams',
    '$scope',
    '$state'
]

function forumEdit(forumSrv, $stateParams, $scope, $state) {

    var vm = this;

    vm.name = 'forumEdit';
    vm.forum = null;
    vm.submit = submit;

    var crud = null;

    activate();

    //////////////////////////////////////////

    function activate() {
        console.log('controller forumEdit initialization');

        $scope.$watchCollection('forumCtrl.forums', init_forum);
    }

    function init_forum() {
        var forums = $scope.$parent.forumCtrl.forums.data;
        var crt = $stateParams.forum_id;
        if (forums != null) {
            for (var i in forums) {
                if (crt == forums[i].id) {
                    vm.forum = forums[i];
                    console.log(vm.forum);
                }
            }
        }
    }

    function submit() {
        if (vm.forum.name != null) {
            var data = "/" + vm.forum.id + "?name=" + vm.forum.name +
                "&user_id=" + vm.forum.user_id;

            forumSrv.update(data, function (d) {
                console.log(d);
            });
        }
        $state.go("^");
    }
}