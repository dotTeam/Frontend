angular
    .module('app.core.shared.forum')
    .controller('forum', forum);

forum.$inject = [
    'forumSrv',
    'forums',
    'forum',
    'urls',
    'postSrv',
    '$scope'
]

function forum(forumSrv, forums, forum, urls, postSrv, $scope) {

    var vm = this;
    vm.name = 'forum';
    vm.forums = [];
    vm.user_id = null;
    vm.forum = forum;
    vm.add = add;
    vm.remove = remove;
    vm.load_more = load_more;
    vm.config = forums;
    vm.token = null;
    vm.img_url = null;
    vm.posts = [];

    var crud = null;
    var last_post = [];
    vm.length = 0;

    activate();

    //////////////////////////////////////////

    function activate() {

        vm.img_url = urls.BASE_API;

        vm.token = window.sessionStorage.getItem("token");
        console.log('controller forum initialization');
        for (var i in forums.data) {
            //console.log(forums.data[i]);
            var forum_local = {
                data: null,
                last_name: null
            };
            forum_local.data = forums.data[i];
            vm.forums.push(forum_local);

        }
        vm.user_id = window.sessionStorage.getItem("user_id");
        crud = new forumSrv.crud();

        load_last_post();
    }

    function load_last_post() {
        for (var i in vm.forums) {
            postSrv.get_last_post(vm.forums[i].data.id,i, function (data,index_forum) {
                var la = index_forum;
                vm.forums[la].last_post = data.data[data.data.length - 1];
                vm.posts.push(data.data);
            })
        }
    }

    function add() {
        crud.save(vm.forum, function (d) {
            if (vm.config.next_page_url == null)
                vm.forums.data.push(d.forum);
            vm.forum.name = null;
            console.log("Succesfuly added");
        });
    }

    function remove(id) {
        console.log(id);
        crud.remove({"id": id}, function (d) {
            console.log(d);
            for (var i in vm.forums.data) {
                if (vm.forums.data[i].id == id) {
                    vm.forums.data.splice(i, 1);
                    break;
                }
            }
        });
    }

    function load_more() {
        if (vm.config.next_page_url) {
            forumSrv.jump_page(vm.config.next_page_url, function (d) {
                for (var i = 0; i < d.data.length; i++) {
                    //console.log(d.data[i]);
                    var forum_local = {
                        data: null,
                        last_name: null
                    };
                    forum_local.data = d.data[i];
                    vm.forums.push(forum_local);
                }
                vm.config = d;
                load_last_post();
            });
        }
    }
}