angular
	.module('app.core.shared.forum')
	.config(forumRoute);

forumRoute.$inject = [
	'$stateProvider'
]

function forumRoute($stateProvider) {
	$stateProvider
		//insert your layout in place of the word LAYOUT
		.state('layout.shared.forum', {
			//rename URL with your url
			url: 'forum',
			templateUrl: 'src/core/shared/forum/forumList.tpl.html',
			controller: 'forum as forumCtrl',
			resolve: {
				forums: ['forumSrv',function (forumSrv) {
					return forumSrv.get_all(handle_reponse);
				}],
				forum: function () {
					return {
						id:null,
						name: null,
						user_id:null
					};
				}
			}
		})
		.state('layout.shared.forum.edit', {
			url: '/:forum_id',
			templateUrl: 'src/core/shared/forum/edit.tpl.html',
			controller: 'forumEdit as forumEditCtrl',
		})

	function handle_reponse(d) {
		return d;
	}
}
