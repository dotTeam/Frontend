angular
	.module('app.core.shared.conversation')
	.factory('conversation', conversation);

conversation.$inject = [
	'$resource',
	'$http',
	'urls'
]

function conversation($resource, $http, urls) {
	var service = {
		crud: crud,
		get: get
	}

	return service;

	////////////////////////////////////////////////////////

	function crud() {
		//replace the word 'WORD' with you resource api endpoint
		return $resource(urls.BASE_API + '/conversation/:id', {id: '@_id'}, {
			remove: {
    			method: 'DELETE'
			}
		})
	}
	function get(id, success) {
		//replace the word 'WORD' with you resource api endpoint
		//this function also has an id to get a specific resource
		return $http.get(urls.BASE_API + '/conversation/' + id)
			.success(success)
	}
	
}