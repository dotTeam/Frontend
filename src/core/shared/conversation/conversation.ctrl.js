angular
    .module('app.core.shared.conversation')
    .controller('conversation', conversation);

conversation.$inject = [
    'conversation',
    'autoSrv',
    '$interval',
    '$state',
    '$http'
]

function conversation(conversation, autoSrv, $interval, $state, $http) {

    var vm = this;
    vm.name = 'message';
    vm.conversations = [];
    vm.my_user_id = null;

    vm.users = [];

    vm.remove = remove;
    vm.add = add;
    vm.add_user = add_user;
    vm.clear_user = clear_user;

    vm.token = null;

    var crud;


    activate();

    //////////////////////////////////////////

    function activate() {
        console.log('controller conversation initialization');

        ////init the auto
        //$state.go("layout.shared.conversation.auto");

        vm.token = window.sessionStorage.getItem("token");

        vm.my_user_id = window.sessionStorage.getItem("user_id");

        crud = new conversation.crud();

        var func1 = function(){
            crud.get(null, function (d) {
                    vm.conversations = d.conversation;
                    get_new_message();
                }
            );
        };

        func1();
        var timer = $interval(func1,10000);
    }

    function get_new_message() {
        for (var i in vm.conversations) {
            for (var j in vm.conversations[i].user) {
                if (vm.conversations[i].user[j].id == vm.my_user_id) {

                    var seen = Date.parse(vm.conversations[i].user[j].pivot.updated_at.replace(' ', 'T'));
                    var conversation_update = Date.parse(vm.conversations[i].updated_at.replace(' ', 'T'));

                    if (seen > conversation_update) {
                        vm.conversations[i].seen = true;
                    }
                    if (conversation_update > seen) {
                        vm.conversations[i].seen = false;
                    }
                }
            }
        }
    }

    function add() {

        var name = "";
        var users = [];

        for(var i in vm.users){
            name += vm.users[i].value + ",";
            users.push(vm.users[i].id);
        }
        name+=window.sessionStorage.getItem("user_name");

        var conversation = {
            name: name,
            users:users
        }
        crud.save(conversation,function(d){
            console.log(d);
            vm.users = [];
        });
    }

    function remove(conversation_id){
        crud.remove({id:conversation_id},function(d){
            console.log(d.message);
        })
    }

    function add_user(){
        var user = autoSrv.get_item().item_select;
        if(user !=null && user.id != vm.my_user_id){
            vm.users.push(user);
        }
    }

    function clear_user(){
        vm.users = [];
    }
}
