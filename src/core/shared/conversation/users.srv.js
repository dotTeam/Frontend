angular
	.module('app.core.shared.conversation')
	.factory('usersSrv', usersSrv);

usersSrv.$inject = [
	'$resource',
	'$http',
	'urls'
]

function usersSrv($resource, $http, urls) {
	var service = {
		crud: crud,
		get: get
	}

	return service;

	////////////////////////////////////////////////////////

	function crud() {
		//replace the word 'WORD' with you resource api endpoint
		return $resource(urls.BASE_API + '/user/:id', {id: '@_id'}, {
			update: {
    			method: 'PUT'
			}
		})
	};
	function get(id, success) {
		//replace the word 'WORD' with you resource api endpoint
		//this function also has an id to get a specific resource
		return $http.get(urls.BASE_API + '/WORD/' + id)
			.success(success)
	}
}