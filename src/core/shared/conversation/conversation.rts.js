angular
    .module('app.core.shared.conversation')
    .config(conversationRoute);

conversationRoute.$inject = [
    '$stateProvider'
]

function conversationRoute($stateProvider) {
    $stateProvider
        .state('layout.shared.conversation', {
            url: 'message',
            views: {
                "": {
                    templateUrl: 'src/core/shared/conversation/conversation.tpl.html',
                    controller: 'conversation as conversationCtrl'
                },
                "auto@layout.shared.conversation": {
                    templateUrl: 'src/common/features/autocomplete/auto.tpl.html',
                    controller: 'auto as autoCtrl'
                }
            }
        })
        //.state('layout.shared.conversation.auto', {
        //    views: {
        //        //"auto@layout.shared.conversation": {
        //        //    templateUrl: 'src/common/features/autocomplete/auto.tpl.html',
        //        //    controller: 'auto as autoCtrl'
        //        //},
        //        "auto@layout.shared.conversation": {
        //            template: "aaaa"
        //        }
        //    }
        //})
}
