angular
	.module('app.core.shared.conversation.message')
	.factory('messageSrv', messageSrv);

messageSrv.$inject = [
	'$resource',
	'$http',
	'urls'
]

function messageSrv($resource, $http, urls) {
	var service = {
		crud: crud,
		get: get,
		load_more:load_more
	}

	return service;

	////////////////////////////////////////////////////////

	function crud() {
		//replace the word 'WORD' with you resource api endpoint
		return $resource(urls.BASE_API + '/message', {id: '@_id'}, {
			update: {
    			method: 'PUT'
			}
		})
	};
	function get(id, success) {
		return $http.get(urls.BASE_API + '/message/?conversation_id=' + id)
			.success(success)
	}
	function load_more(conversation_id,page_id,success){
		return $http.get(urls.BASE_API + '/message/?conversation_id=' + conversation_id + '&page=' + page_id)
			.success(success);
	}
}