angular
	.module('app.core.shared.conversation.message')
	.config(messageRoute);

messageRoute.$inject = [
	'$stateProvider'
]

function messageRoute($stateProvider) {
	$stateProvider
		.state('layout.shared.conversation.message', {
			url: '/:conversation_id',
			templateUrl: 'src/core/shared/conversation/message/message.tpl.html',
			controller: 'message as messageCtrl'
        })
}
