angular
    .module('app.core.shared.conversation.message')
    .controller('message', message);

message.$inject = [
    'messageSrv',
    '$stateParams',
    '$scope',
    'urls'
]

function message(messageSrv, $stateParams, $scope, urls) {

    var vm = this;
    vm.name = 'message';
    vm.messages = [];
    vm.send = send;
    vm.user_name = null;
    vm.user_seen = null;
    vm.users_conversation = null;
    vm.my_user_id = null;

    vm.load_more = load_more;
    vm.show_load_more = false;

    var message_previous = 1;

    var conversation_id;
    var crud;

    vm.token = null;

    vm.message = '';

    activate();

    //////////////////////////////////////////

    function activate() {
        vm.my_user_id = window.sessionStorage.getItem("user_id");

        vm.img_url = urls.BASE_API;

        vm.user_name = window.sessionStorage.getItem("user_name");

        vm.token = window.sessionStorage.getItem("token");

        conversation_id = $stateParams.conversation_id;

        crud = new messageSrv.crud();

        $scope.$watchCollection("conversationCtrl.conversations", load_conversation);
    }

    function load_conversation() {
        var conversations = $scope.$parent.conversationCtrl.conversations;
        var crt = $stateParams.conversation_id;

        if (conversations != null) {
            for (var i in conversations) {
                if (conversations[i].id == crt) {
                    conversations[i].seen = true;

                    vm.user_seen = load_user_seen(conversations[i].user, conversations[i].updated_at);

                    messageSrv.get(conversation_id, function (d) {
                        if(d.next_page_url == null){
                            vm.show_load_more = false;
                        }else{
                            if(message_previous == 1)
                                vm.show_load_more = true;
                        }
                        load_messages(d.data.reverse(), conversations[i].user);
                    });
                    break;
                }
            }
        }
    }

    function load_user_seen(users, updated_at) {
        vm.users_conversation = users;

        var conv_upate = new Date(updated_at);
        var result = [];
        for (var i in users) {
            var user_update = new Date(users[i].pivot.updated_at);
            if (user_update > conv_upate) {
                result.push(users[i]);
            }
        }
        return result;
    }

    function load_messages(messages, users) {
        for (var i in messages) {
            var message = {
                data: messages[i],
                user_name: load_user(users, messages[i].user_id)
            }
            if (vm.messages != null) {
                var data1 = Date.parse(message.data.created_at.replace(' ', 'T'));
                if (vm.messages[vm.messages.length - 1] != null) {
                    var data2 = Date.parse(vm.messages[vm.messages.length - 1].data.created_at.replace(' ', 'T'));
                    if (data1 > data2) {
                        vm.messages.push(message);
                    }else{
                        //everything is all right
                    }
                } else {
                    vm.messages.push(message);
                }
            }
        }
    }

    function load_user(users, message_user_id) {
        for (var i in users) {
            if (users[i].id == message_user_id) {
                return users[i].name;
            }
        }
    }

    function load_more() {
        var conversations = $scope.$parent.conversationCtrl.conversations;
        var users = null;

        for (var i in conversations) {
            if (conversations[i].id == $stateParams.conversation_id) {
                users = conversations[i].user;
            }
        }

        messageSrv.load_more(conversation_id, message_previous + 1, function (d) {
            var messages = d.data;
            message_previous++;
            if (d.next_page_url == null || d.data == []) {
                vm.show_load_more = false;
            }

            for (var i in messages) {
                vm.messages.unshift({
                    data: messages[i],
                    user_name: load_user(users, messages[i].user_id)
                });
            }
        });
    }

    function send() {
        var message = {
            conversation_id: conversation_id,
            message: vm.message
        }

        crud.save(message, function (d) {
            var save_m = {
                data: d.message,
                user_name: vm.user_name
            }
            vm.messages.push(save_m);
            vm.user_seen = [{
                name: vm.user_name
            }];
            vm.message = "";
        });
    }
}