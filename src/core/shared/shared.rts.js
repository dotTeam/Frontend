angular
	.module('app.core.shared')
	.config(sharedRoute);

sharedRoute.$inject = [
	'$stateProvider'
]

function sharedRoute($stateProvider) {
	$stateProvider
		//insert your layout in place of the word 
		.state('layout.shared', {
			//rename URL with your url
			abstract:true,
			//url: 'shared',
			views: {
				'main-content': {
					templateUrl: 'src/core/shared/content.tpl.html',
					controller: 'shared as sharedCtrl',
				}
			},
		})
		.state('layout.shared.page', {
			url: 'page_not_found',
			templateUrl: 'src/core/login/page_not_found.html',
			controller:'login as loginCtrl'
		})
}
