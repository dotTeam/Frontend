angular
	.module('app.core.shared.course')
	.factory('coursePulicSrv', coursePulicSrv);

coursePulicSrv.$inject = [
	'$resource',
	'$http',
	'urls'
]

function coursePulicSrv($resource, $http, urls) {
	var service = {
		subscribe: subscribe
	}

	return service;

	////////////////////////////////////////////////////////

	function subscribe(id,data, success) {
		//replace the word 'WORD' with you resource api endpoint
		//this function also has an id to get a specific resource
		return $http.post(urls.BASE_API + '/course/'+id+'/user',data)
			.success(success)
	}
}