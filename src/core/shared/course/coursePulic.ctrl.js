angular
    .module('app.core.shared.course')
    .controller('coursePulic', coursePulic);

coursePulic.$inject = [
    'coursePulicSrv',
    '$stateParams',
    'courseServ',
    '$state'
]

function coursePulic(coursePulicSrv, $stateParams, courseServ, $state) {

    var vm = this;
    vm.name = 'coursePulic';
    vm.course = null;
    vm.subscribe = subscribe;


    activate();

    //////////////////////////////////////////

    function activate() {
        console.log('controller coursePulic initialization');

        courseServ.get_course_by_id($stateParams.course_id,
            function (data) {
                vm.course = data;
            });
    }

    function subscribe() {
        var data = {
            user_id: window.sessionStorage.getItem("user_id"),
            role: "student"
        }
        coursePulicSrv.subscribe($stateParams.course_id, data, function (d) {
            $state.go("layout.student.course", {course_id: $stateParams.course_id});
        });
    }
}