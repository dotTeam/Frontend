angular
	.module('app.core.shared.course')
	.config(coursePulicRoute);

coursePulicRoute.$inject = [
	'$stateProvider'
]

function coursePulicRoute($stateProvider) {
	$stateProvider
		//insert your layout in place of the word LAYOUT
		.state('layout.shared.course', {
			//rename URL with your url
			url: 'public/course/:course_id',
			templateUrl: 'src/core/shared/course/coursePulic.tpl.html',
			controller: 'coursePulic as coursePulicCtrl'
		})
}
