angular
	.module('app.core.shared.settings')
	.config(settingsRoute);

settingsRoute.$inject = [
	'$stateProvider'
]

function settingsRoute($stateProvider) {
	$stateProvider
		.state('layout.shared.settings', {
			url: 'settings',
			templateUrl: 'src/core/shared/settings/settings.tpl.html',
			controller: 'settings as settingsCtrl'
		})
}
