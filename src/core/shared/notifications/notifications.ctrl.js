angular
	.module('app.core.shared.notifications')
	.controller('notifications', notifications);

notifications.$inject = [
	'notificationsSrv',
	'socketSrv',
	'$scope',
	'urls'
]

function notifications(notificationsSrv, socketSrv, $scope, urls) {

	var vm = this;
	vm.name = 'notifications';
	vm.notifications = [];
	vm.img_url = null;

	vm.token = null;
	
	activate();

	//////////////////////////////////////////

	function activate() {
		console.log('controller notifications initialization');

		vm.img_url = urls.BASE_API;
		vm.token = window.sessionStorage.getItem('token');

		socketSrv.register_observer(function(data){
			vm.notifications.unshift(data.notification);
			$scope.$digest();
		});

		notificationsSrv.get(function(data) {
			vm.notifications = data.notification.notification;
		});

	}
}