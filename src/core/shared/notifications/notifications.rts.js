angular
	.module('app.core.shared.notifications')
	.config(notificationsRoute);

notificationsRoute.$inject = [
	'$stateProvider'
]

function notificationsRoute($stateProvider) {
	$stateProvider
		//insert your layout in place of the word LAYOUT
		.state('layout.shared.notifications', {
			//rename URL with your url
			url: 'notifications',
			templateUrl: 'src/core/shared/notifications/notifications.tpl.html',
			controller: 'notifications as notificationsCtrl',
			//rename PARAM with your param
			params: {PARAM: null,}
		})
}
