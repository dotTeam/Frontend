angular
	.module('app.core.shared.notifications')
	.factory('notificationsSrv', notificationsSrv);

notificationsSrv.$inject = [
	'$http',
	'urls'
]

function notificationsSrv($http, urls) {

	var service = {
		get: get,
		get_count:get_count,
	}

	return service;

	////////////////////////////////////////////////////////

	function get(success) {
		//replace the word 'WORD' with you resource api endpoint
		//this function also has an id to get a specific resource
		return $http.get(urls.BASE_API + '/notification')
			.success(success)
	}
	function get_count(success){
		return $http.get(urls.BASE_API + '/notification-count')
			.success(success);
	}

}