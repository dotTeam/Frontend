angular
    .module('app.core.shared', [
        'app.core.shared.conversation',
        'app.core.shared.calendar',
        'app.core.shared.forum',
        'app.core.shared.notifications',
        'app.core.shared.course',
        'app.core.shared.settings'
    ])
