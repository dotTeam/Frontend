angular
    .module('app.core.shared.calendar')
    .controller('calendar', calendar);

calendar.$inject = [
    'calendarSrv',
    '$stateParams',
    'usersSrv'
]

function calendar(calendarSrv, $stateParams, usersSrv) {

    var vm = this;
    var events =[];

    vm.nname ="";
    vm.ndescription= "";
    vm.nsdate ="";
    vm.nedate= "";


    //////////////////////////////////////////
    activate();

    function activate() {
        console.log("init");


        calendarSrv.getEvents().success(function (data) {
            addCalendar(data);
        });
    }

    function addCalendar(events){
        var newEv = [];
        for(var i = 0; i < events.event.length; i++){
            var ev = {
                title: events.event[i].name,
                start: events.event[i].start_date,
                end: events.event[i].end_date,
                id: events.event[i].id,
                frequency: events.event[i].frequency,
                user_id: events.event[i].user_id,
                description: events.event[i].description
            };
            newEv.push(ev);
        }

        $("#calendar").fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            editable: true,
            events:newEv,
            eventClick: function(calEvent) {

                console.log(calEvent);
                vm.name = calEvent.title;
                vm.sdate = calEvent._start._i;
                vm.id = calEvent.id;
                vm.user_id = calEvent.user_id;
                vm.frequency = calEvent .frequency;
                vm.description = calEvent.description;
                if(calEvent._end != null){
                    vm.edate = calEvent._end._i;
                }

                console.log( vm.name);
                $('#evname').val(vm.name);
                $('#evstart').val(vm.sdate);
                $('#evend').val(vm.edate);

                // change the border color just for fun
                $(this).css('border-color', 'white');
                $('#editEventModal').modal('show');

            },
            eventRender: function(event, element) {
                element.find(".fc-content").append( "<span class='closeon' style='float:right'>X</span>" );
                element.find(".closeon").click(function() {
                    //delete event
                    $('#calendar').fullCalendar('removeEvents',event._id);
                    var data = "/" +  event.id;
                    calendarSrv.deleteEvents(data,function(d){
                        console.log(d);
                    });
                });
            },
            dayClick: function (date) {
                console.log(date);
                //add event
                $('#addEventModal').modal('show');
            }

        });
    }
    vm.update_ev = update_ev;

    function update_ev(){
        var data ="/" + vm.id + "?name=" + vm.name + "&?description=" + vm.description + "&?frequency=" + vm.frequency + "&start_date=" +vm.sdate + "&end_date=" + vm.edate;

          calendarSrv.updateEvents(data,function(d){
             console.log(d);
          });
    }

    vm.save = save;

    function save() {
        console.log("save");

        var data = {
            name: vm.nname,
            description: vm.ndescription,
            frequency: "1",
            start_date: vm.nsdate,
            end_date: vm.nedate,
            group_id: "1"
        };

        console.log(data);
        calendarSrv.save(data, function (data) {
            logger.success('Succesfuly saved', data, ' saved');
        });
    }
}
