angular
	.module('app.core.shared.calendar')
	.factory('calendarSrv', calendarSrv);

calendarSrv.$inject = [
	'$resource',
	'$http',
	'urls'
]

function calendarSrv($resource, $http, urls) {
	var service = {
        getEvents: getEvents,
        success: success,
        updateEvents: updateEvents,
        deleteEvents: deleteEvents,
        save:save
	};

	return service;

    function getEvents(){
        return $http.get(urls.BASE_API + "/event")
            .success(service.success)

    }

    function updateEvents(data){
        return $http.put(urls.BASE_API + "/event" + data)
            .success(service.success);
    }

    function deleteEvents(data){
        return $http.delete(urls.BASE_API + "/event" + data)
            .success(service.success);
    }

    function save(data){
        return $http.post(urls.BASE_API + '/event',data)
            .success(service.success)
    }

    function success(response) {
        return response;
    }
}