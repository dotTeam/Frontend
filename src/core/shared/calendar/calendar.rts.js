angular
	.module('app.core.shared.calendar')
	.config(calendarRoute);

calendarRoute.$inject = [
	'$stateProvider'
]

function calendarRoute($stateProvider) {
	$stateProvider
		//insert your layout in place of the word LAYOUT
		.state('layout.shared.calendar', {
			//rename URL with your url
			url: 'calendar',
			templateUrl: 'src/core/shared/calendar/calendar.tpl.html',
			controller: 'calendar as calendarCtrl',
			params: {PARAM: null}
		})
}
