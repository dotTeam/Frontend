angular
	.module('app.core.shared')
	.controller('shared', shared);

shared.$inject = [
	'$stateParams'
]

function shared($stateParams) {

	var vm = this;
	vm.name = 'shared';

	activate();

	//////////////////////////////////////////

	function activate() {
		console.log('controller shared initialization');
	}
}