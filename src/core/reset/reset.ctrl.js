/**
 * Created by Chris on 10-Nov-15.
 */
angular
    .module("app.core.reset",[])
    .controller("reset",reset);

reset.$inject = [
    '$location',
    'passwordReset',
    'logger',
    '$state'
]

function reset($location,passwordReset,logger){
    var vm = this;

    vm.submit = submit;

    function submit(){

        var token = $location.search().token;

        var formData = {
            token: token,
            email: vm.email,
            password: vm.password,
            password_confirmation: vm.passwordConfirm
        };
        console.log(formData);
        return passwordReset.reset(formData).success(function (data) {
            logger.success('everything is good',data,'good');
            $location.url('/home');
        })
            .error(function(response){
                if(response.email)
                    logger.error('Error because',response.email);
                if(response.password)
                    logger.error('Error because',response.password);

            });
    }
}