/**
 * Created by Chris on 10-Nov-15.
 */
angular
    .module('app.core.reset')
    .config(resetRoute);

resetRoute.$inject = [
    '$stateProvider'
]
function resetRoute($stateProvider) {
    $stateProvider
        .state('reset',{
            url: '/reset?token',
            templateUrl: 'src/core/reset/reset.tpl.html',
            controller: 'reset as resetCtrl',
            "data": {
                requireLogin: false
            }
        })
}