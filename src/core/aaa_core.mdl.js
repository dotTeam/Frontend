angular
    .module('app.core', [
        'app.core.dashboard',
        'app.core.teacher',
        'app.core.login',
        'app.core.forgot',
        'app.core.reset',
        'app.core.admin',
        'app.core.student',
        //shared features
        'app.core.shared'
    ])