/**
 * Created by Chris on 05-Nov-15.
 */
angular
    .module('app.core.teacher')
    .config(teacherRoute);

teacherRoute.$inject = [
    '$stateProvider'
]

function teacherRoute($stateProvider) {

    $stateProvider
        .state('layout.teacher', {
            url: 'teacher',
            views: {
                'main-content': {
                    templateUrl: 'src/core/teacher/content.tpl.html',
                    //controller: 'teacher as teacherCtrl'
                }
            }
        })
}