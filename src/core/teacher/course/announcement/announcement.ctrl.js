angular
    .module('app.core.teacher.course.announcement')
    .controller('announcement', announcement);

announcement.$inject = [
    'announcementSrv',
    '$stateParams',
    'urls'
]

function announcement(announcementSrv, $stateParams, urls) {

    var vm = this;
    vm.name = 'announcement';
    vm.announcements = [];

    vm.title = null;
    vm.description = null;

    vm.add = add;
    vm.remove = remove;
    vm.token = null;

    var crud = null;

    activate();

    //////////////////////////////////////////

    function activate() {
        console.log('controller announcement initialization');
        var crt = $stateParams.course_id;

        vm.img_url = urls.BASE_API;
        vm.token = window.sessionStorage.getItem("token");

        crud = new announcementSrv.crud();

        announcementSrv.get(crt, function (data) {
                console.log(data);
                vm.announcements = data.course.announcement_course;
            }
        );
    }

    function add() {
        var announcement = {
            title: vm.title,
            description: vm.description,
            course_id: $stateParams.course_id
        }
        crud.save(announcement,function(d){
            console.log(d);
            vm.announcements.push(d.announcement);
        });
    }

    function remove(id){
        crud.remove({id:id},function(d){
            for(var i in vm.announcements){
                if(vm.announcements[i].id == id){
                    vm.announcements.splice(i,1);
                    console.log(d.message);
                    break;
                }
            }
        });
    }
}