angular
    .module('app.core.teacher.course.announcement')
    .config(announcementRoute);

announcementRoute.$inject = [
    '$stateProvider'
]

function announcementRoute($stateProvider) {
    $stateProvider
        .state('layout.teacher.course.edit.announcement', {
            url: '/announcement',
            views: {
                "course_role_module@layout.teacher.course.edit": {
                    templateUrl: 'src/core/teacher/course/announcement/announcement.tpl.html',
                    controller: 'announcement as announcementCtrl'
                }
            }
        })
    //.state('layout.teacher.course.edit.announcement', {
    //    url: '/announcement',
    //    views: {
    //        //if you want the option to not be in the tab content
    //        'course_role_module@layout.teacher.course.edit': {
    //            templateUrl: 'src/core/teacher/course/announcement/announcement.tpl.html',
    //            controller: 'announcement as announcementCtrl'
    //        }
    //    }
    //})
}
