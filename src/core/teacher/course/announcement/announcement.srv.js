angular
	.module('app.core.teacher.course.announcement')
	.factory('announcementSrv', announcementSrv);

announcementSrv.$inject = [
	'$resource',
	'$http',
	'urls'
]

function announcementSrv($resource, $http, urls) {
	var service = {
		crud: crud,
		get: get
	}

	return service;

	////////////////////////////////////////////////////////

	function crud() {
		//replace the word 'WORD' with you resource api endpoint
		return $resource(urls.BASE_API + '/announcement-course/:id', {id: '@_id'}, {
			remove: {
    			method: 'DELETE'
			}
		})
	};
	function get(id, success) {
		//replace the word 'WORD' with you resource api endpoint
		//this function also has an id to get a specific resource
		return $http.get(urls.BASE_API + '/course/'+id+'/announcement-course')
			.success(success)
	}
}