angular
	.module('app.core.teacher.course.announcement.comment')
	.controller('comment', comment);

comment.$inject = [
	'commentSrv',
	'$stateParams',
	'urls'
]

function comment(commentSrv, $stateParams, urls) {

	var vm = this;

	vm.name = 'comment';
	vm.comments = [];

	vm.text = null;


	vm.add = add;
	vm.remove = remove;
	vm.img_url = null;

	var crud=null;
	vm.token = null;

	activate();

	//////////////////////////////////////////

	function activate() {
		console.log('controller comment initialization');

		vm.img_url = urls.BASE_API;
		vm.token = window.sessionStorage.getItem("token");

		crud = new commentSrv.crud();
		//crud.get(null, function(data) {
		//	vm.comments = data.comment;
		//	}
		//);
		commentSrv.get($stateParams.announcement_id,function(d){
			vm.comments = d.announcement_course.comment_announcement_course;
		});
	}

	function add() {
		var comment = {
			text: vm.text,
			announcement_course_id: $stateParams.announcement_id
		}
		crud.save(comment,function(d){
			vm.comments.push(d.comment_announcement);
			vm.text = null;
		});
	}

	function remove(id){
		crud.remove({id:id},function(d){
			for(var i in vm.comments){
				if(vm.comments[i].id == id){
					vm.comments.splice(i,1);
					console.log(d.message);
					break;
				}
			}
		});
	}
}