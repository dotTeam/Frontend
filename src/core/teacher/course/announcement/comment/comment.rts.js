angular
	.module('app.core.teacher.course.announcement.comment')
	.config(commentRoute);

commentRoute.$inject = [
	'$stateProvider'
]

function commentRoute($stateProvider) {
	$stateProvider
		//insert your layout in place of the word LAYOUT
		.state('layout.teacher.course.edit.announcement.comment', {
			//rename URL with your url
			url: '/:announcement_id/comment',
			templateUrl: 'src/core/teacher/course/announcement/comment/comment.tpl.html',
			controller: 'comment as commentCtrl'
		})
}