angular
    .module('app.core.teacher.course')
    .controller('edit', edit);

edit.$inject = [
    '$stateParams',
    '$scope',
    'courseServ'
]

function edit($stateParams, $scope, courseServ) {

    var vm = this;
    //vm.name = 'loading name';
    //vm.description = 'loading description';
    vm.course_id = null;

    vm.update_course = update_course;

    activate();

    //////////////////////////////////////////

    function activate() {
        console.log('controller edit initialization');

        vm.course_id = $stateParams.course_id;
        $('#editor').trumbowyg();

        $scope.$watchCollection('courseCtrl.courses', load_course_data);
    }

    function load_course_data() {
        var courses = $scope.$parent.courseCtrl.courses;
        var id = $stateParams.course_id;

        for (var i in courses) {
            if (courses[i].id == id) {
                vm.name = vm.name_local = courses[i].name;
                vm.description = vm.description_local = courses[i].description;
                $('#editor').trumbowyg('html', vm.description);
            }
        }
    }

    function update_course() {
        vm.description_local = $('#editor').trumbowyg('html');
        var data = "/" + $stateParams.course_id;
        data += "?name=" + vm.name_local + "&description=" + vm.description_local;
        courseServ.update_course(data,function(d){
            console.log(d);
            vm.name = d.course.name;
            vm.description = d.course.description;
        });
    }
}