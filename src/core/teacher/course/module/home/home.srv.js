/**
 * Created by Chris on 18-Nov-15.
 */
angular
    .module("app.core.teacher.course.module.home")
    .factory("moduleCrud", moduleCrud);

moduleCrud.$inject = [
    '$resource',
    '$http',
    'urls',
];

function moduleCrud($resource, $http, urls) {

    var service = {
        crud: crud,
        getmodule: getmodule
    }

    return service;

    ////////////////////////////

    function crud() {
        return $resource(urls.BASE_API + '/module/:id', {id: '@_id'}, {
                update: {
                    method: 'PUT'
                }
            }
        );
    }

    function getmodule(id,success) {
        return $http.get(urls.BASE_API + "/course/" + id + "/module")
            .success(success);
    }
}