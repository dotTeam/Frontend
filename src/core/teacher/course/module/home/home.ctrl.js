/**
 * Created by Chris on 18-Nov-15.
 */
angular
    .module("app.core.teacher.course.module.home", [
        'ngSanitize'
    ])
    .controller("moduleHome", moduleHome);

moduleHome.$inject = [
    'moduleCrud',
    '$stateParams',
    '$scope'
];

function moduleHome(moduleCrud, $stateParams, $scope) {
    var vm = this;

    vm.module = null;
    vm.module_id = null;
    vm.course_id = null;
    vm.course_name = null;

    vm.started_at = null;
    vm.submit = submit;

    activate();

    function activate() {
        vm.module_id = $stateParams.module_id;
        vm.course_id = $stateParams.course_id;

        moduleCrud.getmodule(vm.course_id, vm.module_id, function (d) {
            vm.module = d;
            vm.name_edit = d.name;
            vm.started_at_edit = new Date(d.started_at);
            vm.started_at = new Date(d.started_at);
        });

        $scope.$watchCollection('courseCtrl.courses',load_course);
    }
    function load_course(){
        var courses = $scope.$parent.courseCtrl.courses;
        if(courses!=null){
            for(var i in courses){
                if(courses[i].id == $stateParams.course_id){
                    vm.course_name = courses[i].name;
                }
            }
        }
    }

    function submit() {
        if (vm.module != null && vm.module.id != null) {
            var data = "/" + vm.module.id + "/?name=" + vm.name_edit +
                "&started_at=" + vm.started_at_edit.toISOString().substring(0, 10);

            moduleCrud.update(data, function (d) {
                console.log(d);
                vm.module.name = vm.name_edit;
                vm.started_at = vm.started_at_edit;
            })
        }
    }
}