/**
 * Created by Chris on 18-Nov-15.
 */
angular
    .module("app.core.teacher.course.module.home")
    .config(moduleRoute)

moduleRoute.$inject = [
    '$stateProvider'
]

function moduleRoute($stateProvider){
    $stateProvider
        .state('layout.teacher.course.home', {
            url: '/:course_id/module/:module_id',
            templateUrl:"src/core/teacher/course/module/home/home.tpl.html",
            controller:"moduleHome as moduleHomeCtrl"
        })
}