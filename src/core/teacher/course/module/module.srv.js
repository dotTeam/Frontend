/**
 * Created by Chris on 18-Nov-15.
 */
angular
    .module("app.core.teacher.course.module")
    .factory("moduleCrud", moduleCrud);

moduleCrud.$inject = [
    '$resource',
    '$http',
    'urls'
];

function moduleCrud($resource, $http, urls) {

    var service = {
        crud: crud,
        getmodules: getmodules,
        getmodule: getmodule,
        update: update
    }

    return service;

    ////////////////////////////

    function crud() {
        return $resource(urls.BASE_API + '/module/:id', {id: '@id'}, {
                remove: {
                    method: 'DELETE'
                }
            }
        );
    }

    function update(data,success){
        return $http.put(urls.BASE_API + "/module" + data)
            .success(success);
    }

    function getmodules(id, success) {
        return $http.get(urls.BASE_API + "/course/" + id + "/module")
            .success(success);
    }

    function getmodule(course_id, module_id, success) {
        return $http.get(urls.BASE_API + "/course/" + course_id + "/module")
            .success(function (data) {
                for (var i = 0; i < data.course.module.length; i++) {
                    if (data.course.module[i].id == module_id) {
                        success(data.course.module[i]);
                    }
                }
            });
    }
}