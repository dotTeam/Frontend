/**
 * Created by Chris on 25-Nov-15.
 */

angular
    .module("app.core.teacher.course.module",
    [
        'app.core.teacher.course.module.home',
        'app.core.teacher.course.module.page',
        'app.core.teacher.course.module.material',
        'app.core.teacher.course.module.discussion',
        'app.core.teacher.course.module.homework',
        'app.core.teacher.course.module.test'
    ]);