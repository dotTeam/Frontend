/**
 * Created by Chris on 25-Nov-15.
 */
angular
    .module("app.core.teacher.course.module.material")
    .config(materialRoute);

materialRoute.$inject = [
    '$stateProvider'
];

function materialRoute($stateProvider){
    $stateProvider
        .state("layout.teacher.course.home.material",{
            url: '/material',
            templateUrl:"src/core/teacher/course/module/material/material.tpl.html",
            controller:"material as materialCtrl"
        })

}