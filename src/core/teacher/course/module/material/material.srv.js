/**
 * Created by Chris on 25-Nov-15.
 */
angular
    .module("app.core.teacher.course.module.material")
    .factory("materialSrv", materialSrv)

materialSrv.$inject = [
    '$http',
    "urls"
];

function materialSrv($http, urls) {

    var service = {
        postMaterial: postMaterial,
        module_materials: module_materials,
        remove:remove
    }

    return service;

    function postMaterial(data, success, error) {
        var auth = "Bearer " + window.sessionStorage.getItem("token").toString();
        var fd = new FormData();
        fd.append('description', data.description);
        fd.append('module_id', data.module_id);
        fd.append('upload', data.upload);
        $.ajax({
            url: urls.BASE_API + '/material',
            type: 'POST',
            data: fd,
            processData: false,
            contentType: false,
            "headers": {
                "authorization": auth
            },
            success: success,
            error: error
        });
    }

    function module_materials(id, success) {
        return $http.get(urls.BASE_API + "/module/" + id + "/material")
            .success(success);
    }

    function remove(id,success){
        return $http.delete(urls.BASE_API + "/material/" + id)
            .success(success);
    }
}