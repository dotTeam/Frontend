/**
 * Created by Chris on 25-Nov-15.
 */
angular
    .module("app.core.teacher.course.module.material", [])
    .controller("material", material)

material.$inject = [
    'materialSrv',
    '$scope',
    'urls'
];

function material(materialSrv, $scope, urls) {

    var vm = this;
    var module = null;


    vm.materials = [];

    vm.img_url = null;
    vm.token = null;

    vm.remove = remove;

    vm.material = {
        description: null,
        module_id: null,
        upload: null
    };


    vm.submit = submit;

    activate();

    function activate() {
        vm.url = urls.BASE_API + "/material";
        $scope.$watch("moduleHomeCtrl.module", init_page);

        vm.img_url = urls.BASE_API;
        vm.token = window.sessionStorage.getItem("token");

    }

    function init_page() {
        vm.module = $scope.$parent.moduleHomeCtrl.module;
        if (vm.module != null) {
            vm.material.module_id = vm.module.id;
            materialSrv.module_materials(vm.module.id, function (d) {
                vm.materials = d.module.material;
            });
        }
    }

    function submit() {
        materialSrv.postMaterial(vm.material, function (data) {
                if (data.material != null) {
                    console.log("Fisierul a fost incarcat cu success:");
                    vm.materials.push(data.material);
                    $scope.$digest();
                    vm.material.description = null;
                    vm.material.module_id = null;
                }
            }
        ,function(d){
                console.log(d.responseText);
            });
    }

    function remove(id){
        materialSrv.remove(id,function(d){
            for(var i in vm.materials){
                if(vm.materials[i].id == id){
                    vm.materials.splice(i,1);
                }
            }
            console.log(d);
        })
    }

}