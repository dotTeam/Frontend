angular
    .module('app.core.teacher.course.module.homework')
    .factory('homeworkSrv', homeworkSrv);

homeworkSrv.$inject = [
    '$resource',
    '$http',
    'urls'
]

function homeworkSrv($resource, $http, urls) {
    var service = {
        crud: crud,
        get_module_homeworks: get_module_homeworks,
        get_answer_homeworks: get_answer_homeworks,
        add_answer: add_answer,
        add_file: add_file,
        remove_answer: remove_answer
    }

    return service;

    ////////////////////////////////////////////////////////

    function crud() {
        //replace the word 'WORD' with you resource api endpoint
        return $resource(urls.BASE_API + '/homework/:id', {id: '@_id'}, {
            update: {
                method: 'PUT'
            }
        })
    }

    function get_module_homeworks(id, success) {
        return $http.get(urls.BASE_API + '/module/' + id + '/homework')
            .success(success)
    }

    function get_answer_homeworks(id, success) {
        return $http.get(urls.BASE_API + '/homework/' + id + '/homework-answer')
            .success(success)
    }

    function add_answer(data, success) {
        return $http.post(urls.BASE_API + '/homework-answer', data)
            .success(success);
    }

    function add_file(data, success, error) {
        var auth = "Bearer " + window.sessionStorage.getItem("token").toString();
        var fd = new FormData();
        fd.append('upload', data.upload);
        fd.append('homework_id', data.homework_id);
        $.ajax({
            url: urls.BASE_API + '/homework-answer',
            type: 'POST',
            data: fd,
            processData: false,
            contentType: false,
            "headers": {
                "authorization": auth
            },
            success: success,
            error: error
        });
    }

    function remove_answer(id, success) {
        return $http.delete(urls.BASE_API + '/homework-answer/' + id)
            .success(success);
    }
}