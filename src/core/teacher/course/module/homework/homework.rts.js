angular
	.module('app.core.teacher.course.module.homework')
	.config(homeworkRoute);

homeworkRoute.$inject = [
	'$stateProvider'
]

function homeworkRoute($stateProvider) {
	$stateProvider
		//insert your layout in place of the word LAYOUT
		.state('layout.teacher.course.home.homework', {
			//rename URL with your url
			url: '/homework',
			templateUrl: 'src/core/teacher/course/module/homework/homework.tpl.html',
			controller: 'homework as homeworkCtrl',
		})
}
