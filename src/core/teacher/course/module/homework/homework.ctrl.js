angular
    .module('app.core.teacher.course.module.homework')
    .controller('homework', homework);

homework.$inject = [
    'homeworkSrv',
    '$stateParams',
    'urls'
]

function homework(homeworkSrv, $stateParams, urls) {

    var vm = this;
    vm.name = 'homework';
    vm.homeworks = [];

    vm.addhomework = addhomework;
    vm.removehomework = removehomework;

    var crud = null;

    activate();

    //////////////////////////////////////////

    function activate() {
        vm.img_url = urls.BASE_API;
        vm.token = window.sessionStorage.getItem('token');
        console.log('controller homework initialization');

        reset_home_work();

        crud = new homeworkSrv.crud();

        homeworkSrv.get_module_homeworks($stateParams.module_id, function (data) {
                for(var i in data.module.homework){
                    homeworkSrv.get_answer_homeworks(data.module.homework[i].id,function(d){
                        vm.homeworks.push(d.homework);
                    })
                }
                //vm.homeworks = data.module.homework;
            }
        );
    }

    function reset_home_work() {

        //for development
        //vm.homework = {
        //    name: "Homework Chris",
        //    description: "description for event",
        //    points: 2.2,
        //    type: "type1",
        //    required: 0,
        //    answer_type: "upload",
        //    start_date: new Date("2016-01-03"),
        //    end_date: new Date("2016-05-04"),
        //    deadline: new Date("2016-05-04"),
        //    module_id: $stateParams.module_id,
        //}

        //uncomment when you are done
        vm.homework = {
        	name:null,
        	description:null,
        	points:null,
        	type:null,
        	required:null,
        	answer_type:null,
        	start_date:new Date(),
        	end_date:new Date(),
        	deadline:new Date(),
        	module_id:$stateParams.module_id,
        }
    }

    function addhomework() {
        var homework = {
            name: vm.homework.name,
            description: vm.homework.description,
            points: vm.homework.points,
            type: vm.homework.type,
            required: vm.homework.required,
            answer_type: vm.homework.answer_type,
            start_date: vm.homework.start_date.toISOString().substring(0, 10),
            end_date: vm.homework.end_date.toISOString().substring(0, 10),
            deadline: vm.homework.deadline.toISOString().substring(0, 10),
            module_id: vm.homework.module_id
        };

        crud.save(homework, function (data) {
            vm.homeworks.push(data.homework);
            reset_home_work();
            $('#addHomeworkModal').modal('hide');
        });
    }

    function removehomework(id) {
        crud.remove({"id": id}, function (data) {
            console.log(data);
            for (var i in vm.homeworks) {
                if (vm.homeworks[i].id == id) {
                    vm.homeworks.splice(i, 1);
                    break;
                }
            }
        });
    }
}