angular
	.module('app.core.teacher.course.module.test.question')
	.config(questionRoute);

questionRoute.$inject = [
	'$stateProvider'
]

function questionRoute($stateProvider) {
	$stateProvider
		.state('layout.teacher.course.home.question', {
			url: '/question/:test_id',
			templateUrl: 'src/core/teacher/course/module/test/question/question.tpl.html',
			controller: 'question as questionCtrl'
		})
}
