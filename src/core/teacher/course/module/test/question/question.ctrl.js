angular
    .module('app.core.teacher.course.module.test.question')
    .controller('question', question);

question.$inject = [
    'questionSrv',
    '$stateParams',
    '$sce',
    '$scope'
]

function question(questionSrv, $stateParams, $sce, $scope) {

    var vm = this;
    vm.name = 'question';
    vm.questions = [];
    vm.question = null;
    vm.test_name = null;

    vm.addquestion = addquestion;

    var answer = null;
    var answer_correct = null;

    var crud = null;
    var ids = Date.now();

    vm.remove = remove;
    function remove(id) {
        crud.remove({"id": id}, function (d) {
            console.log(d);
            for (var i in vm.questions) {
                if (vm.questions[i].id == id) {
                    vm.questions.splice(i, 1);
                }
            }
        });
    }

    activate();

    //////////////////////////////////////////

    function activate() {
        console.log('controller question initialization');
        load_elements();

        answer = {data: [], type: null};
        answer_correct = {data: [], type: null};
        crud = new questionSrv.crud();
        questionSrv.get($stateParams.test_id, function (data) {
                console.log(data);
                for (var i in data.test.question) {
                    data.test.question[i].answer = JSON.parse(data.test.question[i].answer);
                    data.test.question[i].correct_answer = JSON.parse(data.test.question[i].correct_answer);
                    vm.questions.push(data.test.question[i]);
                }
            }
        );
        $scope.$watchCollection("testCtrl.tests",get_test_name);
    }

    function get_test_name(){
        if($scope.$parent.testCtrl!=null)
        {
            var tests = $scope.$parent.testCtrl.tests;
            if(tests!=null){
                for(var i in tests){
                    if(tests[i].id == $stateParams.test_id){
                        vm.test_name = tests[i].name;
                    }
                }
            }
        }
    }

    function load_elements() {
        answer = {data: [], type: null, question_id: null};
        answer_correct = {data: [], type: null, question_id: null};

        vm.question = {
            question: "2",
            description: "2",
            answer: null,
            correct_answer: null,
            test_id: $stateParams.test_id
        }
        $(".inner").empty();
    }

    vm.clear = clear;
    function clear() {
        answer = {data: [], type: null, question_id: null};
        answer_correct = {data: [], type: null, question_id: null};
        //ids = 0;
        vm.type = 0;
        $(".inner").empty();
    }

    vm.type = 0;
    vm.text_element = text_element;
    function text_element() {
        var local = 0;
        ids++;
        local = ids;
        if (vm.type != 1) {
            clear();
            vm.type = 1;
            answer.type = 'text';
            answer.question_id = ids;
            answer_correct.question_id = ids;
        }
        var text = "<input type='text'" +
            "id='t" + ids + "'>";
        answer.data.push({id: ids, type: 'input-text'});
        answer_correct.data.unshift({data: '', id: "" + ids + ""});
        $(".inner").append(text);

        $("#t" + ids).keyup(function () {
            for (var i in answer_correct.data) {
                if (answer_correct.data[i].id == "" + local + "") {
                    answer_correct.data[i].data = $(this).val();
                }
            }
        });
    }

    vm.checkbox_element = checkbox_element;
    function checkbox_element() {
        var local = 0;
        ids++;
        local = ids;

        if (vm.type != 2) {
            clear();
            vm.type = 2;

            answer.question_id = ids;
            answer_correct.question_id = ids;
            answer.type = 'checkbox';
        }

        answer.data.push({
            id: local,
            data: '',
            type: 'checkbox'
        });

        var text = "<input type='checkbox' " +
            "name='checkbox'" +
            "id='c_a" + ids + "'" +
            ">" +
            "<input type='text' id='c" + ids + "'><br>";
        $(".inner").append(text);

        $("#c" + ids).keyup(function () {
            for (var i in answer.data) {
                if (answer.data[i].id == local) {
                    answer.data[i] = {
                        id: local,
                        data: $(this).val(),
                        type: 'checkbox'
                    };
                }
            }
        });
        $("#c_a" + ids).change(function () {
            if ($("#c_a" + local).is(":checked")) {
                answer_correct.data.push({
                    id: "" + local + ""
                });
            } else {
                for (var i in answer_correct.data) {
                    if (answer_correct.data[i].id == local) {
                        answer_correct.data.splice(i, 1);
                    }
                }
            }
        });
    }


    vm.radio_element = radio_element;
    function radio_element() {

        var local = 0;
        var ok = 0;
        ids++;
        local = ids;

        if (vm.type != 4) {
            ok = 1;
            clear();
            vm.type = 4;

            answer_correct.question_id = ids;
            answer_correct.data = [];
            answer_correct.data.push({
                id: "" + local + ""
            });
            answer.question_id = ids;
            answer.type = 'radio';
        }


        answer.data.push({
            id: local,
            data: '',
            type: 'radio'
        });

        var text = "";
        text += "<input type='radio' " +
            "name='radio'" +
            "id='r_a" + ids + "'";
        if (ok == 1) {
            text += "checked='checked'";
        }
        text += "/>"
        text += "<input type='text' id='r" + ids + "'" +
            "><br>";

        $(".inner").append(text);

        $("#r" + ids).keyup(function () {
            for (var i in answer.data) {
                if (answer.data[i].id == local) {
                    answer.data[i] = {
                        id: local,
                        data: $(this).val(),
                        type: 'radio'
                    };
                }
            }
        });
        $("#r_a" + ids).change(function () {
            if ($("#r_a" + local).is(":checked")) {
                answer_correct.data = [];
                answer_correct.data.push({
                    id: "" + local + ""
                });
            }
        });
    }

    vm.add_text = add_text;
    function add_text(text) {
        if (vm.type == 0) {
            answer.type = 'text';
            answer.question_id = ids;
            answer_correct.question_id = ids;
        }
        vm.type = 1;
        answer.data.push({data: text, type: 'text'});
        $(".inner").append("<p>" + text + "</p>");
    }

    vm.addquestion = addquestion;
    function addquestion() {
        answer_correct.data.sort(function (a, b) {
            return parseInt(a.id) - parseInt(b.id);
        });

        vm.question.answer = JSON.stringify(answer);
        vm.question.correct_answer = JSON.stringify(answer_correct);


        crud.save(vm.question, function (d) {
            console.log(d.question);
            d.question.answer = JSON.parse(d.question.answer);
            d.question.correct_answer = JSON.parse(d.question.correct_answer);
            vm.questions.push(d.question);
        });
        vm.question = null;
        vm.type = 0;

        load_elements();

    }

    //vm.select_element = select_element;
    //function select_element() {
    //    //in the next version
    //    var text = '';
    //
    //    if (type != 3) {
    //        clear();
    //        type = 3;
    //        text = "<input type='text' value='option'>" +
    //            "<button type='button' " +
    //            "onclick='console.log(1)'>" +
    //            "Add option</button><br>" +
    //            "<select></select>";
    //    }
    //    $(".inner").append(text);
    //}
    //
    //vm.option_element = option_element;
    //function option_element(option){
    //    console.log(option);
    //}
}