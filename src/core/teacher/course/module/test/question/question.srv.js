angular
    .module('app.core.teacher.course.module.test.question')
    .factory('questionSrv', questionSrv);

questionSrv.$inject = [
    '$resource',
    '$http',
    'urls'
]

function questionSrv($resource, $http, urls) {
    var service = {
        crud: crud,
        get: get,
        submit_answer: submit_answer
    }

    return service;

    ////////////////////////////////////////////////////////

    function crud() {
        //replace the word 'WORD' with you resource api endpoint
        return $resource(urls.BASE_API + '/question/:id', {id: '@_id'}, {
            remove: {
                method: 'DELETE'
            }
        })
    };
    function get(id, success) {
        return $http.get(urls.BASE_API + '/test/' + id + '/question')
            .success(success)
    }

    function submit_answer(data, success) {
        return $http.post(urls.BASE_API + '/question-answer', data)
            .success(success)
    }
}