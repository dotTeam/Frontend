angular
    .module('app.core.teacher.course.module.test')
    .controller('test', test);

test.$inject = [
    'testSrv',
    '$stateParams',
    'urls'
]

function test(testSrv, $stateParams, urls) {

    var vm = this;
    vm.tests = [];

    vm.addtest = addtest;
    vm.test = null;
    var user_data = JSON.parse(window.sessionStorage.getItem('userdata'));
    vm.img_url = urls.BASE_API;
    vm.token = window.sessionStorage.getItem('token');
    vm.remove = remove;
    vm.addtest = addtest;

    var crud = null;

    activate();

    //////////////////////////////////////////

    function activate() {
        console.log('controller test initialization');

        reset_home_work();

        crud = new testSrv.crud();

        testSrv.get_test($stateParams.module_id, function (data) {
                vm.tests = data.module.test;
            }
        );
    }

    function reset_home_work() {
        vm.test = {
            name: "Test name",
            description: "description for test",
            required: 0,
            duration: 0,
            start_date: new Date("2016-01-03"),
            end_date: new Date("2016-05-04"),
            deadline: new Date("2016-05-04"),
            module_id: $stateParams.module_id,
        }

        //vm.homework = {
        //	name:null,
        //	description:null,
        //	required:null,
        //	start_date:new Date(),
        //	end_date:new Date(),
        //	deadline:new Date(),
        //	module_id:$stateParams.module_id,
        //}
    }

    function addtest() {
        //TODO: add duration of the test
        console.log(vm.test);
        crud.save(vm.test, function (data) {
            console.log(data.test);
            data.test['user'] = user_data;
            vm.tests.push(data.test);
            $('#myModal').modal('hide');
        })
    }

    function remove(id) {
        crud.remove({"id": id}, function (d) {
            console.log(d);
            for (var i in vm.tests) {
                if (vm.tests[i].id == id) {
                    vm.tests.splice(i, 1);
                }
            }
        });
    }
}