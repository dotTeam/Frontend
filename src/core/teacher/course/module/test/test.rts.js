angular
	.module('app.core.teacher.course.module.test')
	.config(testRoute);

testRoute.$inject = [
	'$stateProvider'
]

function testRoute($stateProvider) {
	$stateProvider
		//insert your layout in place of the word LAYOUT
		.state('layout.teacher.course.home.test', {
			//rename URL with your url
			url: '/test',
			templateUrl: 'src/core/teacher/course/module/test/test.tpl.html',
			controller: 'test as testCtrl'
		})
}
