angular
	.module('app.core.teacher.course.module.test')
	.factory('testSrv', testSrv);

testSrv.$inject = [
	'$resource',
	'$http',
	'urls'
]

function testSrv($resource, $http, urls) {
	var service = {
		crud: crud,
		get_test: get_test
	}

	return service;

	////////////////////////////////////////////////////////

	function crud() {
		//replace the word 'WORD' with you resource api endpoint
		return $resource(urls.BASE_API + '/test/:id', {id: '@_id'}, {
			remove: {
    			method: 'DELETE'
			}
		})
	};
	function get_test(id, success) {
		return $http.get(urls.BASE_API + '/module/' + id + '/test?id=!-51&with=user')
			.success(success)
	}
}