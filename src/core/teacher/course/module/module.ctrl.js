/**
 * Created by Chris on 18-Nov-15.
 */
angular
    .module("app.core.teacher.course.module")
    .controller("module", module);

module.$inject = [
    '$state',
    'moduleCrud',
    '$stateParams',
    'courseServ'
];

function module($state, moduleCrud, $stateParams, courseServ) {
    var vm = this;

    vm.moduleName = "";
    vm.moduleStartDate;
    vm.course;
    vm.addContent = addContent;
    vm.remove = remove;
    var ModuleCrud;

    vm.submitModule = submitModule;

    vm.modules = [];

    vm.back = back;

    activate();

    function activate() {
        vm.moduleStartDate = new Date();
        ModuleCrud = new moduleCrud.crud();

        courseServ.get_course_by_id($stateParams.course_id,
            function (data) {
                vm.course = data;

                if (vm.course.id != null) {
                    moduleCrud.getmodules(vm.course.id, function (data) {

                        vm.modules = data.course.module;
                    });
                }
            });
    }

    function back() {
        $state.go('layout.teacher.course');
    }

    function submitModule() {

        if (vm.moduleName != "" && vm.course.id != null) {

            //get the date
            var data = vm.moduleStartDate.toISOString().substring(0, 10);

            var module = {
                name: vm.moduleName,
                course_id: vm.course.id,
                started_at: data
            };

            ModuleCrud.save(module, function (data) {
                vm.modules.push(data.module);
                console.log(data.module);
            }, function (data) {
                console.log("nu am voie sa adaug un modul acestui curs,deoarece " + data.message);
            });

            vm.moduleName = "";
        }
    }

    function remove(id) {
        ModuleCrud.remove({"id": id}, function (d) {
            console.log(d.message);
            for (var i in vm.modules) {
                if (vm.modules[i].id == id) {
                    vm.modules.splice(i, 1);
                    break;
                }
            }
        });
    }

    function addContent(module) {
        //$state.go("layout.teacher.course.edit.home", {module_id: module.id});
        $state.go("layout.teacher.course.home.page", {module_id: module.id, course_id: $stateParams.course_id});
    }
}