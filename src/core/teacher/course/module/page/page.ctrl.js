/**
 * Created by Chris on 19-Nov-15.
 */
angular
    .module("app.core.teacher.course.module.page", [])
    .controller("page", page);

page.$inject = [
    "$state",
    "$scope",
    "pageCrud"
];

function page($state, $scope, pageCrud) {
    var vm = this;

    vm.page = null;
    vm.module = null;

    var PageCrud = null;
    var save_flag = null;
    var page_id = null;

    vm.save = save;
    vm.back = back;

    activation();

    ///////////////////////////////////

    function activation() {

        //module page
        PageCrud = new pageCrud.crud();

        $('#editor').trumbowyg();
        $scope.$watch("moduleHomeCtrl.module", init_page);
    }

    function init_page() {
        vm.module = $scope.$parent.moduleHomeCtrl.module;
        if (vm.module != null) {
            save_flag = false;
            vm.page = {
                name: "Page1",
                content: "<p>Write your page content in here</p>",
                module_id: vm.module.id
            }

            pageCrud.get_page(vm.module.id, function (data) {

                var pages = data.module.page_module;
                console.log(data);
                //only take the first page

                if (pages[0] != null) {
                    page_id = pages[0].id;
                    save_flag = true;
                    vm.page.name = pages[0].name;
                    vm.page.content = pages[0].content;
                }

                $('#editor').trumbowyg('html', vm.page.content);
            })
        }
    }


    function back() {
        $state.go('^');
    }

    function save() {

        if (save_flag == false) {
            save_flag = true;
            PageCrud.save(vm.page);
        } else {
            pageCrud.update_page(page_id, vm.page).success(function (data) {

                })
                .error(function (data) {
                });
        }

        vm.page.content = $('#editor').trumbowyg('html');
    }
}