/**
 * Created by Chris on 19-Nov-15.
 */
angular
    .module("app.core.teacher.course.module.page")
    .config(pageRoute);

pageRoute.$inject = [
    '$stateProvider'
];

function pageRoute($stateProvider){
    $stateProvider
        .state("layout.teacher.course.home.page",{
            url: '/page',
            templateUrl:"src/core/teacher/course/module/page/page.tpl.html",
            controller:"page as pageCtrl"
        })

}