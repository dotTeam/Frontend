/**
 * Created by Chris on 19-Nov-15.
 */
angular
    .module("app.core.teacher.course.module.page")
    .factory("pageCrud", pageCrud);

pageCrud.$inject = [
    '$resource',
    '$http',
    'urls'
];

function pageCrud($resource, $http, urls) {

    var service = {
        crud: crud,
        update_page: update_page,
        get_page: get_page
    }

    return service;

    ////////////////////////////

    function crud() {
        return $resource(urls.BASE_API + '/page-module', {id: '@id'}, {
                update: {
                    method: 'PUT'
                }
            }
        );
    }

    function update_page(id,data) {
        return $http.put(urls.BASE_API + '/page-module/' + id,data);
    }

    function get_page(id, success) {
        return $http.get(urls.BASE_API + "/module/" + id + "/page")
            .success(success);
    }
}