angular
    .module('app.core.teacher.course.module.discussion')
    .factory('discussionSrv', discussionSrv);

discussionSrv.$inject = [
    '$resource',
    '$http',
    'urls'
]

function discussionSrv($resource, $http, urls) {
    var service = {
        crud: crud,
        get: get,
        get_comments: get_comments,
        post_comment: post_comment,
        post_discussion: post_discussion,
        remove_discussion:remove_discussion,
        remove_comment:remove_comment
    }

    return service;

    ////////////////////////////////////////////////////////

    function crud() {
        //replace the word 'WORD' with you resource api endpoint
        return $resource(urls.BASE_API + '/WORD/:id', {id: '@_id'}, {
            update: {
                method: 'PUT'
            }
        })
    };
    function get(id, success) {
        //replace the word 'WORD' with you resource api endpoint
        //this function also has an id to get a specific resource
        return $http.get(urls.BASE_API + '/module/' + id + '/discussion?id=!-51&with=user')
            .success(success)
    }

    function get_comments(id,index_disscusion, success) {
        //replace the word 'WORD' with you resource api endpoint
        //this function also has an id to get a specific resource
        return $http.get(urls.BASE_API + '/discussion/' + id + '/comment?id=!-51&with=user')
            .success(function(data){success(data,index_disscusion);})
    }

    function post_comment(data, success) {
        return $http.post(urls.BASE_API + '/comment', data)
            .success(success);
    }

    function post_discussion(data, success) {
        return $http.post(urls.BASE_API + '/discussion', data)
            .success(success);

    }

    function remove_discussion(id,success){
        return $http.delete(urls.BASE_API + "/discussion/" + id)
            .success(success);
    }

    function remove_comment(id,success){
        return $http.delete(urls.BASE_API + "/comment/" + id)
            .success(success);
    }
}