angular
	.module('app.core.teacher.course.module.discussion')
	.config(discussionRoute);

discussionRoute.$inject = [
	'$stateProvider'
]

function discussionRoute($stateProvider) {
	$stateProvider
		//insert your layout in place of the word LAYOUT
		.state('layout.teacher.course.home.discussion', {
			//rename URL with your url
			url: '/discussion',
			templateUrl: 'src/core/teacher/course/module/discussion/discussion.tpl.html',
			controller: 'discussion as discussionCtrl',
		})
}
