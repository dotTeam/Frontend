/**
 * Created by Chris on 15-Nov-15.
 */
angular
    .module("app.core.teacher.course",[
        "app.core.teacher.course.module",
        "app.core.teacher.course.role",
        "app.core.teacher.course.announcement"
    ])