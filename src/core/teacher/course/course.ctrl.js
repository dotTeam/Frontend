/**
 * Created by Chris on 18-Nov-15.
 */
angular
    .module("app.core.teacher.course")
    .controller("courseTeacher", course);

course.$inject = [
    "$state",
    "courseServ"
];

function course($state, courseServ) {
    var vm = this;

    vm.courses = [];
    vm.addModule = addModule;

    vm.get_courses = get_courses;
    vm.user_role = null;

    vm.courseName = "";
    vm.courseDescription = "";

    vm.saved = false;

    vm.save = save;
    vm.remove = remove;

    var crud = new courseServ.crud();

    activate();

    //////////////////////////////////////////

    function activate() {
        //get all courses
        vm.user_role = window.sessionStorage.getItem("role");

        courseServ.getmycourses(function (data) {
            for(var i in data.user.course){
                if(data.user.course[i].pivot.role == "teacher"){
                    vm.courses.push(data.user.course[i]);
                }
            }
        });
    }


    function addModule(course) {
        console.log(course);
        //$state.go('layout.module', {course_id: course.id});
    }

    function get_courses(id){
        vm.courses = [];
        if(id == 1){
            //we get all the courses
            crud.get(function(data){
                var courses = data.course;
                courseServ.getmycourses(function (data) {
                    for (var i in data.user.course) {
                        vm.courses.push(data.user.course[i]);
                    }
                    var diff = [];
                    var ok = 0;
                    for(var i in courses){
                        ok = 0;
                        for(var j in vm.courses){
                            if(courses[i].id==vm.courses[j].id){
                                ok = 1;
                            }
                        }
                        if(ok == 0){
                            diff.push(courses[i]);
                        }
                    }
                    for(var i in diff){
                        vm.courses.push(diff[i]);
                    }
                });
            });
        }
        if(id == 2){
            //courses that i am subscribed
            courseServ.getmycourses(function (data) {
                vm.courses = data.user.course;
            });
        }
        if(id == 3){
            //courses that i am a teacher
            courseServ.getmycourses(function (data) {
                for(var i in data.user.course){
                    if(data.user.course[i].pivot.role == "teacher"){
                        vm.courses.push(data.user.course[i]);
                    }
                }
            });
        }
    }

    function remove(id){
        crud.remove({"id":id},function(data){
            console.log(data);
            for(var i in vm.courses){
                if(vm.courses[i].id == id){
                    vm.courses.splice(i,1);
                }
            }
        });
    }

    function save() {
        vm.saved = true;

        var course = {
            name: vm.courseName,
            description: vm.courseDescription
        }

        crud.save(course, function (data) {
            //logger.success('Succesfuly saved', data.course, 'Course saved');
            data.course['pivot'] = {
                role:'teacher'
            }
            vm.courses.push(data.course);
            //console.log(data.course);
        });
    }
}