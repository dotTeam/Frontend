/**
 * Created by Chris on 16-Nov-15.
 */
angular
    .module('app.core.teacher.course')
    .factory("courseServ", courseServ);

courseServ.$inject = [
    '$resource',
    'urls',
    '$http'
];

function courseServ($resource, urls, $http) {

    var service = {
        crud: crud,
        getmycourses: getmycourses,
        get_course_by_id:get_course_by_id,
        update_course: update_course,
        unsubscribe:unsubscribe,
        get_course_id:get_course_id
    }

    return service;

    //////////////////////////////////////

    function crud() {
        return $resource(urls.BASE_API + '/course/:id', {id: '@_id'}, {
            remove: {
                method: 'DELETE'
            }
        });
    }

    function getmycourses(success) {

        return $http.get(urls.BASE_API + "/user/course")
                .success(success);
    }

    function get_course_by_id(course_id,success){
        $http.get(urls.BASE_API + "/course")
            .success(function(data){
                for(var i in data.course){

                    if(data.course[i].id == course_id){

                        success(data.course[i]);
                    }
                }
            });
    }

    function update_course(data,success){
        return $http.put(urls.BASE_API + "/course" + data)
            .success(success);
    }
    function unsubscribe(data,success){
        return $http.delete(urls.BASE_API + "/course" + data)
            .success(success);
    }

    function get_course_id(id,success){
        return $http.delete(urls.BASE_API + "/course?id=" +id)
            .success(success);
    }
}