angular
    .module('app.core.teacher.course.role')
    .factory('roleSrv', roleSrv);

roleSrv.$inject = [
    '$resource',
    '$http',
    'urls'
]

function roleSrv($resource, $http, urls) {
    var service = {
        crud: crud,
        update: update,
        remove:remove
    }

    return service;

    ////////////////////////////////////////////////////////

    function crud(course_id,success) {
        console.log(urls.BASE_API + '/course/' + course_id + '/user');
        //replace the word 'WORD' with you resource api endpoint
        return $resource(urls.BASE_API + '/course/' + course_id + '/user',success);
    };
    function update(data, success) {
        //replace the word 'WORD' with you resource api endpoint
        //this function also has an id to get a specific resource
        return $http.put(urls.BASE_API + data)
            .success(success)
    };
    function remove(data, success){

        return $http.delete(urls.BASE_API + '/course/' + data.course_id
            + '/user/' + data.user_id)
            .success(success)
    }
}