angular
    .module('app.core.teacher.course.role')
    .controller('role', role);

role.$inject = [
    'roleSrv',
    '$stateParams',
    'autoSrv'
]

function role(roleSrv, $stateParams, autoSrv) {

    var vm = this;
    vm.name = 'role';
    vm.users = [];

    var crud = null;

    vm.addUser = addUser;
    vm.update = update;
    vm.deleteUser = deleteUser;

    activate();

    //////////////////////////////////////////

    function activate() {
        console.log('controller role initialization');
        console.log($stateParams.course_id);
        crud = new roleSrv.crud($stateParams.course_id);
        crud.get($stateParams.course_id, function (data) {
                vm.users = data.cours.user;
            }
        );
    }

    function update(user_id, user_role) {
        if (user_role != null) {
            roleSrv.update("/course/" + $stateParams.course_id + "/user/" +
                user_id + "?role=" + user_role, function (d) {
                console.log(d);
            });
        }
    }

    function addUser() {
        var user = {
            user_id: autoSrv.get_item().item_select.display.id,
            role: "student",
        };
        crud.save(user, function (d) {
            console.log(d);
            var user_auto = autoSrv.get_item().item_select.display;
            var display_user = {
                name: user_auto.name,
                id: user_auto.id,
                pivot: {
                    role: user.role
                }
            }
            vm.users.push(display_user);
        });
    }

    function deleteUser(user_id) {
        console.log(user_id);
        var data = {
            course_id: $stateParams.course_id,
            user_id: user_id
        }
        roleSrv.remove(data, function (d) {
            console.log(d);
            for (var i = 0; i < vm.users.length; i++) {
                if (vm.users[i].id == user_id) {
                    vm.users.splice(i,1);
                    break;
                }
            }
        });
    }
}