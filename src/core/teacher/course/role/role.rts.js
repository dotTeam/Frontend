angular
	.module('app.core.teacher.course.role')
	.config(roleRoute);

roleRoute.$inject = [
	'$stateProvider'
]

function roleRoute($stateProvider) {
	$stateProvider
		//insert your layout in place of the word LAYOUT
		.state('layout.teacher.course.edit.roles', {
			url:"/role",
			views:{
				"course_role_module@layout.teacher.course.edit":{
					templateUrl:"src/core/teacher/course/role/role.tpl.html",
					controller:"role as roleCtrl"
				},
				"auto@layout.teacher.course.edit.roles":{
					templateUrl: 'src/common/features/autocomplete/auto.tpl.html',
					controller: 'auto as autoCtrl'
				}

			}

		})
}
