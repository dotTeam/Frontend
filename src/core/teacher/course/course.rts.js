/**
 * Created by Chris on 15-Nov-15.
 */
angular
    .module("app.core.teacher.course")
    .config(courseRoute)

courseRoute.$inject = [
    '$stateProvider'
]

function courseRoute($stateProvider){
    $stateProvider
        .state('layout.teacher.course', {
            url: '/course',
            templateUrl:"src/core/teacher/course/course.tpl.html",
            controller:"courseTeacher as courseCtrl"
        })
        .state('layout.teacher.course.edit', {
            url: '/:course_id',
            views:{
                "":{
                    templateUrl:"src/core/teacher/course/edit.tpl.html",
                    controller:"edit as editCtrl"
                },
                "course_role_module@layout.teacher.course.edit":{
                    templateUrl:"src/core/teacher/course/module/module.tpl.html",
                    controller:"module as moduleCtrl"
                }
            }

        })
        .state('layout.teacher.course.edit.module', {
            views:{
                "course_role_module@layout.teacher.course.edit":{
                    templateUrl:"src/core/teacher/course/module/module.tpl.html",
                    controller:"module as moduleCtrl"
                }
            }
        })
}
