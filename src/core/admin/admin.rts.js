angular
	.module('app.core.admin')
	.config(adminRoute);

adminRoute.$inject = [
	'$stateProvider'
]

function adminRoute($stateProvider) {

	$stateProvider
		.state('layout.admin', {
			url: 'admin',
			views: {
				'main-content': {
					templateUrl: 'src/core/admin/content.tpl.html',
				}
			}
		})
}
