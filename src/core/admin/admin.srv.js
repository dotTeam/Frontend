angular
	.module('app.core.admin')
	.factory('adminSrv', adminSrv);

adminSrv.$inject = [
	'$resource',
	'$http',
	'urls'
]

function adminSrv($resource, $http, urls) {
	var service = {
		crud: crud,
		get: get
	}

	return service;

	////////////////////////////////////////////////////////

	function crud() {
		//replace the word 'WORD' with you resource api endpoint
		return $resource(urls.BASE_API + '/WORD/:id', {id: '@_id'}, {
			update: {
    			method: 'PUT'
			}
		})
	};
	function get(id, success) {
		//replace the word 'WORD' with you resource api endpoint
		//this function also has an id to get a specific resource
		return $http.get(urls.BASE_API + '/WORD/' + id)
			.success(success)
	}
}