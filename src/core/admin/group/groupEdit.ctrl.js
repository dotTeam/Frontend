angular
    .module('app.core.admin.group')
    .controller('groupEdit', groupEdit);

groupEdit.$inject = [
    'groupSrv',
    '$stateParams',
    'groups',
    'group',
    '$scope',
    '$state',
    'autoSrv',
    'userSrv',
    'urls'
]

function groupEdit(groupSrv, $stateParams, groups, group, $scope, $state, autoSrv, userSrv, urls) {

    var vm = this;

    vm.group = group;
    //the initial values of the group,we store this to see if there are any modifications
    var group_init;
    vm.groups = groups;

    vm.edit = edit;
    vm.addUser = addUser;
    vm.deleteUser = deleteUser;

    vm.users = [];
    vm.token = null;

    var crt;

    activate();

    //////////////////////////////////////////

    function activate() {
        console.log('controller groupEdit initialization');

        vm.img_url = urls.BASE_API;

        crt = $stateParams.group_crt;
        vm.token = window.sessionStorage.getItem('token');

        //watch the parents groups changes
        //to no make another request
        vm.groups = $scope.$parent.groupCtrl.groups;
        $scope.$watchCollection('groupCtrl.groups', load_group_data);
    }

    function load_group_data() {
        //we load the corespondend group
        if (vm.groups[crt] != null) {
            //i do the assignagtion bellow because of the 2 ways data biding
            vm.group = {
                id: vm.groups[crt].data.id,
                name: vm.groups[crt].data.name,
                description: vm.groups[crt].data.description
            }

            //we take the initial values of the group
            group_init = {
                name: vm.group.name,
                description: vm.group.description
            }

            //get all the users
            groupSrv.getGroupUsers(vm.group.id, function (data) {
                vm.users = data.group.user;
            });
        }
    }

    function edit() {
        var url = "/" + vm.group.id + "?";

        if (vm.group.name != null &&
            group_init.name != vm.group.name) {
            url += "name=" + vm.group.name + "&";
        }

        if (vm.group.description != null &&
            group_init.description != vm.group.description) {
            url += "description=" + vm.group.description;
        }
        groupSrv.put(url, function (d) {
            $state.go('layout.admin.group',{},{reload:true});
        });
    }

    function addUser() {
        var user = autoSrv.get_item();

        if (user != null) {
            var data = "/" + user.item_select.id + "?group_id=" + vm.group.id;

            userSrv.put(data, function (d) {
                if (check_user(d.user) == 0) {
                    vm.users.push(d.user);
                }
                else {
                    console.log("user all ready in group");
                }
            });
            //autoSrv.set_item(null);
        }
    }

    function deleteUser(id) {
        //TODO after fixing the backend bug in group_id==0 all the users with no real grup
        var data = "/" + id + "?group_id=0";

        if (id) {
            userSrv.put(data, function (d) {
                console.log(d);
                //daca ii cu success
                if (true == false) {
                    for (var i = 0; i < vm.users.length; i++) {
                        if (vm.users[i].id == id) {
                            vm.users.splice(i,1);
                            break;
                        }
                    }
                }
            });
        }
    }

    function check_user(user) {
        var ok = 0;
        for (var i in vm.users) {
            if (vm.users[i].id == user.id) {
                ok = 1;
            }
        }
        return ok;
    }
}