angular
    .module('app.core.admin.group')
    .controller('group', group);

group.$inject = [
    'groupSrv',
    'groups',
    'group',
    '$state'
]

function group(groupSrv, groups, group, $state) {

    var vm = this;
    vm.groups = groups;
    vm.group = group;

    var crud = new groupSrv.crud();

    vm.add = add;
    vm.remove = remove;

    activate();

    //////////////////////////////////////////

    function activate() {
        console.log('controller1 group initialization');
    }

    function add() {
        crud.save(vm.group,function(d){
            console.log(d.group);
            vm.groups.push(d.group);
            $state.go('layout.admin.group',{},{reload:true});
        });
    }

    function remove(id){
        groupSrv.remove(id,function(d){
            for(var i=0;i<vm.groups.length;i++){
                if(vm.groups[i].data.id == d.id){
                    console.log(i);
                    vm.groups.splice(i,1);
                }
            }
        });
    }
}