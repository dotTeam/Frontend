angular
    .module('app.core.admin.group')
    .factory('groupSrv', groupSrv);

groupSrv.$inject = [
    '$resource',
    '$http',
    'urls',
    '$q'
]

function groupSrv($resource, $http, urls, $q) {
    var service = {
        crud: crud,
        get: get,
        put: put,
        remove: remove,
        getAll: get_all,
        getGroupUsers: getGroupUsers
    }

    return service;

    ////////////////////////////////////////////////////////

    function crud() {
        //replace the word 'WORD' with you resource api endpoint
        return $resource(urls.BASE_API + '/group/:id', {id: '@_id'}, {
            update: {
                method: 'PUT'
            }
        })
    };
    function get(id, success) {
        //replace the word 'WORD' with you resource api endpoint
        //this function also has an id to get a specific resource
        return $http.get(urls.BASE_API + '/WORD/' + id)
            .success(success)
    }

    function get_all(success_handle) {
        var defer = $q.defer();
        $http.get(urls.BASE_API + '/group')
            .success(function (d) {
                defer.resolve(success_handle(d));
            });
        return defer.promise;
    }

    function put(data, success) {
        return $http.put(urls.BASE_API + '/group' + data)
            .success(success)
    }

    function remove(id, success) {
        return $http.delete(urls.BASE_API + '/group/' + id)
                .success(function(d){
                    var response = {
                        id:id,
                        response:d
                    }
                    success(response);
                })
    }

    function getGroupUsers(id, success) {
        console.log(id);
        return $http.get(urls.BASE_API + '/group/' + id + "/user")
            .success(success);
    }
}