angular
    .module('app.core.admin.group')
    .config(groupRoute);

groupRoute.$inject = [
    '$stateProvider'
]

function groupRoute($stateProvider) {

    var handle_groupsAll_reponse = handle_groupsAll_reponse;

    $stateProvider
        .state('layout.admin.group', {
            url: '/groups',
            templateUrl: 'src/core/admin/group/group.tpl.html',
            controller: 'group as groupCtrl',
            resolve: {
                groups: ['groupSrv', function (groupSrv) {
                    return groupSrv.getAll(handle_groupsAll_reponse);
                }],
                group: function () {
                    return {
                        group: {
                            name: 'Loading name',
                            description: 'Loading description'
                        }
                    };
                }
            }
        })

        .state('layout.admin.groupAdd', {
            url: '/group/add',
            templateUrl: 'src/core/admin/group/add.tpl.html',
            controller: 'group as groupCtrl',
            resolve: {
                groups: function () {
                    return [];
                },
                group: function () {
                    return {
                        name: 'Write the name of the group',
                        description: 'Write the description of the group'
                    };
                }
            }
        })

        .state('layout.admin.group.edit', {
            url: '/:group_crt',
            views: {
                "": {
                    templateUrl: 'src/core/admin/group/edit.tpl.html',
                    controller: 'groupEdit as groupEditCtrl',
                    params: {group_crt: null}
                },
                "auto@layout.admin.group.edit":{
                    templateUrl: 'src/common/features/autocomplete/auto.tpl.html',
                    controller: 'auto as autoCtrl'
                }
            },
            resolve: {
                groups: function () {
                    return [];
                },
                group: ['groupSrv', function (groupSrv) {
                    return {
                        name: 'Loading name edit',
                        description: 'Loading edit'
                    };
                }]
            }
        })

    function handle_groupsAll_reponse(d) {
        var result = [];
        for (var i = 0; i < d.group.length; i++) {
            result.push({cont: i, data: d.group[i]});
        }
        return result;
    }
}
