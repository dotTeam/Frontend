angular
	.module('app.core.admin.user')
	.config(userRoute);

userRoute.$inject = [
	'$stateProvider'
]

function userRoute($stateProvider) {
	$stateProvider
		//insert your layout in place of the word LAYOUT
		.state('layout.admin.user', {
			//rename URL with your url
			url: '/user',
			templateUrl: 'src/core/admin/user/user.tpl.html',
			controller: 'user as userCtrl',
			//rename PARAM with your param
			//params: {PARAM: null,}
		})
}
