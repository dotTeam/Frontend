angular
    .module('app.core.admin.user')
    .controller('user', user);

user.$inject = [
    'userSrv',
    '$stateParams',
    'urls'
]

function user(userSrv, $stateParams, urls) {

    var vm = this;

    vm.users = [];

    vm.token = null;

    activate();

    //////////////////////////////////////////

    function activate() {
        //console.log('controller user initialization');
        vm.img_url = urls.BASE_API;
        vm.token = window.sessionStorage.getItem('token');

        var crud = new userSrv.crud();
        crud.get(function (d) {
            for (var i = 0; i < d.user.length; i++) {
                vm.users.push({
                    data: d.user[i],
                    value: d.user[i].name.toLowerCase(),
                    id: d.user[i].id
                });
            }
        })
    }
}