angular
    .module('app.core.admin.user.crud_user')
    .controller('user_edit', user_edit);

user_edit.$inject = [
    '$stateParams',
    '$scope',
    'userSrv',
    'groupSrv',
    '$state',
    'Auth'
]

function user_edit($stateParams, $scope, userSrv, groupSrv, $state, Auth) {

    var vm = this;
    vm.name = 'crud_user';
    vm.user = null;
    vm.groups = [];

    var user_init = null;
    var crud;
    var users = [];
    var init_user_data = init_user_data

    vm.submit = submit;

    activate();

    //////////////////////////////////////////

    function activate() {
        console.log('controller user_edit initialization');

        groupSrv.getAll(function (d) {
            vm.groups = d.group;
        });

        crud = new userSrv.crud();

        users = $scope.$parent.userCtrl.users;
        $scope.$watchCollection('userCtrl.users', init_user_data);
    }

    function init_user_data() {
        var crt = $stateParams.user_id;
        if (users != []) {
            for (var i = 0; i < users.length; i++) {
                if (users[i].id == crt) {
                    vm.user = users[i].data;
                    user_init = {
                        email: vm.user.email,
                        registration_number: vm.user.registration_number
                    };
                }
            }
        }
    }

    function submit() {
        var user = {
            name: null,
            first_name: null,
            last_name: null,
            role_id: null,
            group_id: null,
            email: null,
            registration_number: null,
            upload: null,
            password: null,
            use_gravatar: null
        };

        var role_id = 3;
        if (vm.user.role.role == "admin")
            role_id = 1;
        if (vm.user.role.role == "teacher")
            role_id = 2;
        if (vm.user.role.role == "student")
            role_id = 3;

        var data = "/" + vm.user.id + "?";

        if (vm.user.use_gravatar != null) {
            user.use_gravatar = vm.user.use_gravatar;
        }

        if (vm.user.email != user_init.email) {
            data += "email=" + vm.user.email + "&";
            user.email = vm.user.email;
        }
        if (vm.user.registration_number != user_init.registration_number) {
            data += "registration_number=" + vm.user.registration_number + "&";
            user.registration_number = vm.user.registration_number;
        }

        data += "name=" + vm.user.name + "&first_name=" + vm.user.first_name +
            "&last_name=" + vm.user.last_name + "&role_id=" + role_id
            + "&group_id=" + vm.user.group_id;

        user.name = vm.user.name;
        user.first_name = vm.user.first_name;
        user.last_name = vm.user.last_name;
        user.role_id = role_id;
        user.group_id = vm.user.group_id;

        if (vm.user.upload != null) {
            user.upload = vm.user.upload;
        }
        if (vm.user.password != null) {
            user.password = vm.user.password;
        }
        //userSrv.put(data, function (d) {
        //    console.log(d);
        //});
        userSrv.update_user(vm.user.id, user, function (d) {
            //here we update the view
            vm.user = d;
            if (d.user.id == window.sessionStorage.getItem("user_id")) {


                var user_data = update_userdata(JSON.parse(window.sessionStorage.getItem("userdata")), d);

                window.sessionStorage.setItem("user_name", d.user.name);
                window.sessionStorage.setItem("userdata", JSON.stringify(user_data));
            }
            setTimeout(function () {
                location.reload();
            }, 1000);
            $state.go("^");
        });

    }

    function update_userdata(new_user_data, d) {
        new_user_data.name = d.user.name;
        new_user_data.first_name = d.user.first_name;
        new_user_data.last_name = d.user.last_name;
        new_user_data.email = d.user.email;
        new_user_data.registration_number = d.user.registration_number;
        new_user_data.group_id = d.user.group_id;
        return new_user_data;
    }
}