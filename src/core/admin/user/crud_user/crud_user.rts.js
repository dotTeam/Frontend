angular
    .module('app.core.admin.user.crud_user')
    .config(crud_userRoute);

crud_userRoute.$inject = [
    '$stateProvider'
]

function crud_userRoute($stateProvider) {
    $stateProvider
    //insert your layout in place of the word LAYOUT
        .state('layout.admin.user.add', {
            url: '/add',
            templateUrl: 'src/core/admin/user/crud_user/add.tpl.html',
            controller: 'add_user as add_userCtrl'
        })
        .state('layout.admin.user.edit', {
            //rename URL with your url
            url: '/:user_id',
            templateUrl: 'src/core/admin/user/crud_user/edit.tpl.html',
            controller: 'user_edit as user_editCtrl'
        })

}