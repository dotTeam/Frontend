angular
    .module('app.core.admin.user.crud_user')
    .controller('add_user', add_user);

add_user.$inject = [
    '$scope',
    'userSrv',
    'groupSrv',
    '$state'
]

function add_user($scope, userSrv, groupSrv, $state) {


    var vm = this;
    vm.name = 'crud_user';
    vm.user = null;
    vm.groups = [];

    var user_init = null;
    var crud;

    vm.submit = submit;
    vm.add = add;

    activate();

    //////////////////////////////////////////

    function activate() {
        console.log('controller add_user initialization');

        groupSrv.getAll(function(d){
            vm.groups = d.group;
        });

        crud = new userSrv.crud();
    }

    function submit() {

        var role_id = 3;

        if (vm.user.role.role == "admin")
            role_id = 1;
        if (vm.user.role.role == "teacher")
            role_id = 2;
        if (vm.user.role.role == "student")
            role_id = 3;

        var data = "/" + vm.user.id + "?";

        if (vm.user.email != user_init.email)
            data += "email=" + vm.user.email + "&";
        if (vm.user.registration_number != user_init.registration_number)
            data += "registration_number=" + vm.user.registration_number + "&";

        data += "name=" + vm.user.name + "&first_name=" + vm.user.first_name +
            "&last_name=" + vm.user.last_name + "&role_id=" + role_id;

        userSrv.put(data, function (d) {
            console.log(d);
        });
    }

    function add() {

        if (vm.user != null && vm.user !="" && vm.user.role_id !=null) {

            userSrv.add_user(vm.user, function (d) {
                var user = {
                    data: d.user,
                    id: d.user.id
                }
                $scope.$parent.userCtrl.users.push(user);
                $state.go("^");
            });
        }
    }
}