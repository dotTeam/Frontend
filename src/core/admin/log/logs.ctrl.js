angular
	.module('app.core.admin.log')
	.controller('logs', logs);

logs.$inject = [
	'logsSrv',
	'$stateParams',
	'urls'
]

function logs(logsSrv, $stateParams, urls) {

	var vm = this;
	vm.name = 'logs';
	vm.logs = [];

	vm.remove = remove;
	
	activate();

	//////////////////////////////////////////

	function activate() {
		console.log('controller logs initialization');


		vm.img_url = urls.BASE_API;
		vm.token = window.sessionStorage.getItem('token');

		logsSrv.get(function(data) {
			vm.logs = data;
			}
		);
	}

	function remove(){
		vm.logs = [];
		logsSrv.remove(function(d){
			console.log(d.message);
		})
	}
}