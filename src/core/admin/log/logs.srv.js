angular
	.module('app.core.admin.log')
	.factory('logsSrv', logsSrv);

logsSrv.$inject = [
	'$http',
	'urls'
]

function logsSrv($http, urls) {
	var service = {
		remove: remove,
		get: get
	}

	return service;

	////////////////////////////////////////////////////////

	function remove(success) {
		return $http.delete(urls.BASE_API + '/log/api_endpoints')
			.success(success)
	};
	function get(success) {
		return $http.get(urls.BASE_API + '/log/api_endpoints?env=local')
			.success(success)
	}
}