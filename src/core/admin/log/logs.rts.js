angular
	.module('app.core.admin.log')
	.config(logsRoute);

logsRoute.$inject = [
	'$stateProvider'
]

function logsRoute($stateProvider) {
	$stateProvider
		//insert your layout in place of the word LAYOUT
		.state('layout.admin.log', {
			//rename URL with your url
			url: '/logs',
			templateUrl: 'src/core/admin/log/logs.tpl.html',
			controller: 'logs as logsCtrl',
			//rename PARAM with your param
			params: {PARAM: null,}
		})
}
