angular
    .module('app.core.admin', [
        "app.core.admin.user",
        "app.core.admin.group",
        "app.core.admin.log"
    ])
