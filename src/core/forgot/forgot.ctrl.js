/**
 * Created by Chris on 10-Nov-15.
 */
angular
    .module("app.core.forgot", [])
    .controller("forgot", forgot);

forgot.$inject = [
    '$location',
    'emailReset',
    'logger'
]

function forgot($location, emailReset, logger) {
    var vm = this;

    vm.submit = submit;

    function submit() {

        var formData = {
            email: vm.email
        };

        return emailReset.reset(formData).success(function (data) {
            logger.success('everything is good', data, 'good');
        })
            .error(function (data) {
                logger.error(data,'not got');
            });
    }
}