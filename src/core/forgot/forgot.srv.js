/**
 * Created by Chris on 10-Nov-15.
 */
angular
    .module("app.core.forgot")
    .factory("emailReset",emailReset);

emailReset.$inject = [
    '$http',
    'urls',
    'routerHelper'
]

function emailReset($http, urls,routerHelper){
    var service = {
        reset: reset,
        success: success,
        error: error
    };

    return service;

    //////////////////////////////////////////////////

    function reset(data) {
        return $http.post(urls.BASE_API + "/password/email",data)
            .success(service.success)
            .error(service.error);
    }

    function success(response) {
        return response;
    }

    function error(data) {
        routerHelper.handleRoutingErrors();
        //console.log("Error: " + data);
    }
}