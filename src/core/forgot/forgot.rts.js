/**
 * Created by Chris on 10-Nov-15.
 */
angular
    .module('app.core.forgot')
    .config(forgotRoute);

forgotRoute.$inject = [
    '$stateProvider'
]
function forgotRoute($stateProvider) {
    $stateProvider
        .state('forgot',{
            url: '/forgot',
            templateUrl: 'src/core/forgot/forgot.tpl.html',
            controller: 'forgot as forgotCtrl',
            "data": {
                requireLogin: false
            }
        })
}