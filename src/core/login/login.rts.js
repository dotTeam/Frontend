/**
 * Created by Chris on 05-Nov-15.
 */
angular
    .module('app.core.login')
    .config(loginRoute);

loginRoute.$inject = [
    '$stateProvider'
]
function loginRoute($stateProvider) {
    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'src/core/login/login.tpl.html',
            controller: 'login as loginCtrl',
            "data": {
                requireLogin: false
            }
        })
}