/**
 * Created by Chris on 02-Nov-15.
 */
/**
 * Created by Chris on 29-Oct-15.
 */


angular
    .module("app.core.login", [
        //"app.core.forgot"
    ])
    .controller('login', login);

login.$inject = [
    '$location',
    'Auth',
    '$state',
    'urls',
    'notificationsSrv'
];

function login($location, Auth, $state, urls, notificationsSrv) {

    var vm = this;

    vm.submit = submit;
    vm.successAuth = successAuth;
    vm.errorAuth = errorAuth;
    activate();

    function activate() {

        $('body').addClass('login-body');
    }

    function submit() {

        var formData = {
            email: vm.username,
            password: vm.password
        };

        Auth.login(formData, vm.successAuth, vm.errorAuth)

    }

    function successAuth(response) {
        var role = response.user.role.role;

        $('body').removeClass('login-body');

        if (role == "teacher") {
            $state.go("layout.teacher.dashboard", {}, {reload: true});
        }
        if (role == "admin") {
            $state.go("layout.admin.dashboard", {}, {reload: true});
        }
         if (role == "student") {
            $state.go("layout.student.dashboard", {}, {reload: true});
        }
    }

    function errorAuth(response) {

        console.log(response);
        alert("Message: " + response.error);
    }
}
