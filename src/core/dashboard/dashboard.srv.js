/**
 * Created by Chris on 07-Nov-15.
 */
angular
    .module("app.core.dashboard")
    .factory("dashTest", dashTest);
dashTest.$inject = [
    '$http',
    'urls',
    'routerHelper'
];
function dashTest($http, urls, routerHelper) {

    var service = {
        test: test,
        success: success,
        error: error
    };

    return service;

    //////////////////////////////////////////////////

    function test() {
        return $http.get(urls.BASE_API + "/test/Mihai")
            .success(service.success)
            .error(service.error);
    }

    function success(response) {
        return response;
    }

    function error(data) {
        routerHelper.handleRoutingErrors();
        //console.log("Error: " + data);
    }
}