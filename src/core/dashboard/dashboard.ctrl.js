angular
    .module("app.core.dashboard",[

    ])
    .controller("dashboard", dashboard)

dashboard.$inject = [
    'jwtHelper',
    'Auth',
    '$location',
    'logger'
];

function dashboard(jwtHelper, Auth, $location, logger) {

    var vm = this;

    vm.logout = logout;
    vm.test = test;
    var userdata = JSON.parse(sessionStorage.getItem("userdata"));
    var role = userdata.role;


        vm.modules = [
            {title:'Courses', route:'#/teacher/course'},
            {title:'Messages',route:'#/message'},
            {title:'Forum',route:'#/forum'},
            {title:'Events',route:'#/calendar'},
            {title:'Settings',route:'#/settings'}
        ]


    vm.token = sessionStorage.getItem("token");
    vm.token_date = jwtHelper.getTokenExpirationDate(vm.token);
    vm.isExpired = jwtHelper.isTokenExpired(vm.token);

    activate();

    function activate(){
        console.log(role);
        console.log(vm.modules);
    }

    function test() {
        return dashTest.test().success(function (data) {
            logger.success('everything is good',data,'good');
        });
    };
    function logout() {
        Auth.logout(function () {
            $location.path("/home");
        });
    };
}