angular
    .module('app.core.dashboard')
    .config(dashRoute);

dashRoute.$inject = [
    '$stateProvider'
]

function dashRoute($stateProvider) {
    $stateProvider
        .state('layout.teacher.dashboard', {
            url: '/dashboard',
            templateUrl: "src/core/dashboard/dashboard.tpl.html",
            controller: "dashboard as dashCtrl"
        })
        .state('layout.student.dashboard', {
            url: '/dashboard',
            templateUrl: "src/core/dashboard/dashboard.tpl.html",
            controller: "dashboard as dashCtrl"
        })
        .state('layout.admin.dashboard', {
            url: '/dashboard',
            templateUrl: "src/core/dashboard/dashboard.tpl.html",
            controller: "dashboard as dashCtrl"
        });
}