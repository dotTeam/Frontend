angular
    .module('app.layout')
    .controller('sidebar', sidebar);

sidebar.$inject = [
    'notificationsSrv',
    'socketSrv',
    '$scope'
]

function sidebar(notificationsSrv, socketSrv, $scope) {

    var vm = this;
    vm.role;
    vm.nr_notifications = null;

    activate();

    //////////////////////////////////////////

    function activate() {
        var userdata = JSON.parse(sessionStorage.getItem("userdata"));
        vm.role = userdata.role.role;
        vm.nr_notifications = 0;

        //init

        notificationsSrv.get_count(function(data){

            //get the nr of notifications
            vm.nr_notifications+=data.notification_count;

            //socket get notification
            socketSrv.register_observer(function(data){

                if(data.notification.from_id!=userdata.id) {
                    vm.nr_notifications++;
                }

                $scope.$digest();
            });
        });

    }
}