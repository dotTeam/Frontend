/**
 * Created by Chris on 11-Nov-15.
 */
angular
    .module("app.layout")
    .controller("top", top);

top.$inject = [
    'Auth',
    '$location',
    'urls',
    'notificationsSrv',
    'socketSrv',
    '$scope'
]

function top(Auth, $location, urls, notificationsSrv, socketSrv, $scope) {
    var vm = this;

    vm.logout = logout;
    vm.img_url = null;
    vm.nr_notifications = null;
    vm.get_notifications = get_notifications;

    vm.notifications = [];

    activate();

    function activate() {
        vm.img_url = urls.BASE_API;

        var user = JSON.parse(Auth.getCurrentUser());
        vm.username = user.name;
        vm.user_id = user.id;
        vm.token = window.sessionStorage.getItem('token');

        load_notification();
    }

    function load_notification() {
        var userdata = JSON.parse(sessionStorage.getItem("userdata"));

        vm.nr_notifications = 0;

        //init

        notificationsSrv.get_count(function (data) {

            //get the nr of notifications
            vm.nr_notifications += data.notification_count;

            //socket get notification
            socketSrv.register_observer(function (data) {
                if (data.notification.from_id != userdata.id) {
                    vm.nr_notifications++;
                }

                $scope.$digest();
            });
        });
    }

    function logout() {
        Auth.logout(function () {
            $location.path("/home");
        });
    };

    function get_notifications() {
        var limit = 5;
        vm.nr_notifications = 0;
        vm.notifications = [];
        notificationsSrv.get(function (data) {
            for (var i = 0;i<data.notification.notification.length && i < limit; i++) {
                vm.notifications.push(data.notification.notification[i]);
            }
        });
    }
}