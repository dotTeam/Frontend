/**
 * Created by Chris on 11-Nov-15.
 */
angular
    .module("app.layout", [])
    .config(layoutRoute);

layoutRoute.$inject = [
    '$stateProvider'
];

function layoutRoute($stateProvider) {

    $stateProvider
        .state('layout', {
            url:'/',
            views: {
                '@': {
                    templateUrl: "src/layout/layout.tpl.html"
                },
                'top@layout': {
                    templateUrl: "src/layout/top.tpl.html",
                    controller: "top as topCtrl"
                },
                'sidebar@layout': {
                    templateUrl: "src/layout/sidebar.tpl.html",
                    controller: "sidebar as sidebarCtrl"
                }
                ,
                'main@layout': {
                    templateUrl: "src/layout/main.tpl.html"
                }
            },
            "data": {
                requireLogin: true
            },
        })
}