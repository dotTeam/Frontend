/**
 * Created by Chris on 07-Nov-15.
 */
angular
    .module("app.common.services.httpRequestInterceptor", [])
    .factory("httpRequestInterceptor", httpRequestInterceptor);

httpRequestInterceptor.$inject = [
    '$q',
    '$location'
];

function httpRequestInterceptor($q, $location) {

    var service = {
        request: request,
        responseError: responseError
    }

    return service;

    ///////////////////////////////////////////

    function request(config) {

        config.headers = config.headers || {};
        if (window.sessionStorage.getItem('token')) {
            config.headers.Authorization = 'Bearer ' + window.sessionStorage.getItem('token');
        }
        return config;
    }

    function responseError(response) {
        if (response.status === 401 || response.status === 403) {
            //$location.path('/home');
            alert("This operation is forrbiden");
        }
        return $q.reject(response);
    }

}