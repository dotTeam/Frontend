angular
    .module('app.common.services.socket',[])
    .factory('socketSrv', socketSrv);

socketSrv.$inject = [
]

function socketSrv() {
    var service = {
        register_observer: register_observer,
        handle_socket:handle_socket
    }

    var observers = [];

    var socket = null;

    return service;

    ////////////////////////////////////////////////////////

    function register_observer(callback){
        observers.push(callback);
    }

    function notify_observers(data){
        for(var i in observers){
            observers[i](data);
        }
    }

    function handle_socket(socket_param,user_id){
        socket = socket_param;
        socket.on('user.' + user_id + ':App\\Events\\NotifyUserEvent', function (data) {
            //console.log(data);
            notify_observers(data);
            //socketSrv.get(data);
        });
    }
}
