angular
    .module("app.common.services.auth", [
        "app.common.services.socket"
    ])
    .factory('Auth', auth);

auth.$inject = [
    '$http',
    '$localStorage',
    'urls',
    'socketSrv'
];

function auth($http, $localStorage, urls ,socketSrv) {

    var tokenClaims = getClaimsFromToken();

    var socket_config = {
        socket: null,
        token: null,
        user_id: null
    };

    var service = {
        login: login,
        logout: logout,
        getCurrentUser: getCurrentUser,
        getTokenClaims: getTokenClaims
    }

    activate();

    return service;

    /////////////////////////////////////////////////////

    function activate() {
        handle_socket();
    }

    function handle_socket(){
        //here we create the token when the page loads/refresh

        var token = sessionStorage.getItem("token");
        var user_id = sessionStorage.getItem("user_id");

        if (token != null && user_id != null) {

            socket_config.token = token;
            socket_config.user_id = user_id;

            if(socket_config.socket == null){
                socket_config.socket = io(urls.BASE_REAL_TIME);
            }
            listen_socket_backend(socket_config);
        }
    }

    function listen_socket_backend(socket_config_local){
        var socket = socket_config_local.socket;
        socket.on('connect', function () {
            socket.on('authenticated', function () {
                //console.log('auth successfully');
                //socketSrv.get('auth successfully');
                socketSrv.handle_socket(socket,socket_config.user_id);

            }).emit('authenticate', {token: socket_config_local.token});
        });

        socket.on("error", function (error) {
            if (error.type == "UnauthorizedError" || error.code == "invalid_token") {
                console.log("User's token has expired");
            }
        });
    }

    function urlBase64Decode(str) {
        var output = str.replace('-', '+').replace('_', '/');
        switch (output.length % 4) {
            case 0:
                break;
            case 2:
                output += '==';
                break;
            case 3:
                output += '=';
                break;
            default:
                throw 'Illegal base64url string!';
        }
        return window.atob(output);
    }

    function getCurrentUser() {
        return sessionStorage.getItem("userdata");
    }

    function getClaimsFromToken() {
        var token = $localStorage.token;
        //var token = sessionStorage.getItem("token");

        var user = {};
        if (typeof token !== 'undefined') {
            var encoded = token.split('.')[1];
            user = JSON.parse(urlBase64Decode(encoded));
        }
        return user;
    }

    function login(data, success, error) {
        $http.post(urls.BASE_API + '/login', data).success(function (data) {

            set_local_storage(data);

            //we must to succesfully authentificate with the socket
            //setTimeout(handle_socket, 1000);
            handle_socket();
            success(data);

        }).error(error)
    }

    function set_local_storage(data){
        sessionStorage.setItem("userdata", JSON.stringify(data.user));
        sessionStorage.setItem("token", data.user.token);
        sessionStorage.setItem("role", data.user.role.role);
        sessionStorage.setItem("user_id", data.user.id);
        sessionStorage.setItem("user_name", data.user.name);
    }

    function logout(success) {
        tokenClaims = {};

        sessionStorage.clear();
        socket_config.socket.disconnect();

        success();

        //to completly destroy the sockets other the simple disconnect won't work
        location.reload();

    }

    function getTokenClaims() {
        return tokenClaims;
    }

}