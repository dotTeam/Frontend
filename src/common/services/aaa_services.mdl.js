angular
	.module('app.common.services', [
		"app.common.services.socket",
		"app.common.services.auth",
		"app.common.services.httpRequestInterceptor",
		"app.common.services.crud",
		"app.common.services.time"
	])
