/**
 * Created by Chris on 09-Nov-15.
 */
angular
    .module('blocks.logger', [])
    .factory('logger', logger);

logger.$inject = [
    '$log'
    //,'toastr'
];

function logger($log) {
    var service = {
        //showToasts: true,

        error: error,
        info: info,
        success: success,
        warning: warning,

        // straight to console; bypass toastr
        log: $log.log
    };

    return service;
    /////////////////////

    function error(message, data, title) {
        //toastr.error(message, title);
        $log.error('Error: ' + message, data);
    }

    function info(message, data, title) {
        //toastr.error(message, title);
        $log.info('Info: ' + message, data);
    }

    function success(message, data, title) {
        //toastr.error(message, title);
        $log.info('Success: ' + message, data);
    }

    function warning(message, data, title) {
        //toastr.error(message, title);
        $log.warn('Warning: ' + message, data);
    }
}