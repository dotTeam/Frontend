/**
 * Created by Chris on 09-Nov-15.
 */
angular
    .module("blocks.router", [
        'ui.router',
        'blocks.logger'
    ])
    .factory('routerHelper', routerHelper)

routerHelper.$inject = [
    '$location',
    '$rootScope',
    'logger'
];

function routerHelper($location, $rootScope, logger) {

    var handlingStateChangeError = false;

    var stateCounts = {
        errors: 0,
        changes: 0
    };

    var services = {
        handleRoutingErrors: handleRoutingErrors
    }

    return services;

    //////////////////////
    function handleRoutingErrors() {
        // Route cancellation:
        // On routing error, go to the dashboard.
        // Provide an exit clause if it tries to do it twice.
        $rootScope.$on('$stateChangeError',
            function (event, toState, toParams, fromState, fromParams, error) {

                if (handlingStateChangeError) {
                    return;
                }
                stateCounts.errors++;
                handlingStateChangeError = true;
                var destination = (toState &&
                    (toState.title || toState.name || toState.loadedTemplateUrl)) ||
                    'unknown target';
                var msg = 'Error routing to ' + destination + '. ' +
                    (error.data || '') + '. <br/>' + (error.statusText || '') +
                    ': ' + (error.status || '');
                logger.warning(msg, [toState]);
                $location.path('/');
            }
        );
    }
}