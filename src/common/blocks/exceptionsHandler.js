/**
 * Created by Chris on 09-Nov-15.
 */
angular
    .module("blocks.exceptionHandler", [])
    .config(exceptionConfig)

exceptionConfig.$inject = [
    '$provide'
];

function exceptionConfig($provide) {
    $provide.decorator('$exceptionHandler', extendExceptionHandler);
}

extendExceptionHandler.$inject = ['$delegate', 'logger'];

function extendExceptionHandler($delegate, logger) {
    return function (exception, cause) {
        $delegate(exception, cause);
        var errorData = {
            exception: exception,
            cause: cause
        };
        /**
         * Could add the error to a service's collection,
         * add errors to $rootScope, log errors to remote web server,
         * or log locally. Or throw hard. It is entirely up to you.
         * throw exception;
         */
        //toastr.error(exception.msg, errorData);
        logger.error(exception.msg, errorData);
    };
}