/**
 * Created by Chris on 09-Nov-15.
 */
angular
    .module('blocks.exceptionCatch',[])
    .factory('exception', exception);

exception.$inject = [
    'logger'
];

function exception(logger) {
    var service = {
        catcher: catcher
    };
    return service;

    function catcher(message) {
        return function(reason) {
            logger.error(message, reason);
        };
    }
}