angular
    .module("app.common.constants",[])
    .constant('urls', {
        BASE: 'http://colectiv-front.dev/',
        BASE_API: 'http://dot-utwo.rhcloud.com/api/v1',
        BASE_REAL_TIME:'http://colectiv-back.dev:3000'
    });