angular
    .module("app.common.filters.time", [])
    .filter('secondsToDateTime', function () {
        return function (seconds) {
            var d = new Date(0, 0, 0, 0, 0, 0, 0);
            d.setSeconds(seconds);
            return d;
        };
    });