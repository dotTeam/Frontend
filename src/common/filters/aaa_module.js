angular
    .module("app.common.filters", [
        "app.common.filters.timeago",
        "app.common.filters.time"
    ])