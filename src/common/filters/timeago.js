angular
    .module('app.common.filters.timeago',[])
    .filter('timeAgo', ['$interval', function ($interval){
        // trigger digest every 60 seconds
        $interval(function (){}, 60000);

        function fromNowFilter(time){
            return moment(time).fromNow();
        }

        fromNowFilter.$stateful = true;
        return fromNowFilter;
    }]);
