/**
 * Created by Chris on 15-Nov-15.
 */
angular
    .module('app.common.ngEnter', [])
    .directive('ngEnter', function () {
        //nu prefer cu ngEnter deorece pe mobile nu am enter
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.ngEnter, {'event': event});
                    });

                    event.preventDefault();
                }
            });
        };
    });