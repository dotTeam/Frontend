angular
    .module("app.common.filters.timeago",[])
    .directive('timeAgo', ['NotificationService',
        function (NotificationService) {
            return {
                template: '<span>{{timeAgo}}</span>',
                replace: true,
                link: function (scope, element, attrs) {
                    var updateTime = function () {
                        scope.timeAgo = moment(scope.$eval(attrs.timeAgo)).fromNow();
                    }
                    NotificationService.onTimeAgo(scope, updateTime); // subscribe
                    updateTime();
                }
            }
        }])