/**
 * Created by Chris on 09-Nov-15.
 */
angular
    .module("app.common", [
        /*angular modules*/
        'ui.router',
        'angular-jwt',
        'ngStorage',
        'ngResource',
        'ngMaterial',
        'ng',
        'ui.bootstrap',
        //template caching
        'template',

        /*blocks modules*/
        'blocks.exceptionCatch',
        'blocks.exceptionHandler',
        'blocks.logger',
        'blocks.router',

        /*directives modules*/
        'app.common.directives',

        /*services modules*/
        "app.common.services",

        /*constants modules*/
        "app.common.constants",

        /*shared feature*/
        "app.common.features",

        /*shared filters*/
        "app.common.filters"
    ])