angular
	.module('app.common.features')
	.factory('userSrv', userSrv);

userSrv.$inject = [
	'$resource',
	'$http',
	'urls'
]

function userSrv($resource, $http, urls) {
	var service = {
		crud: crud,
		get: get,
		put: put,
		add_user:add_user,
		update_user:update_user
	}

	return service;

	////////////////////////////////////////////////////////

	function crud() {
		//replace the word 'WORD' with you resource api endpoint
		return $resource(urls.BASE_API + '/user', {
			update: {
    			method: 'PUT', params: {id:"@id"}
			}
		})
	};
	function get(id, success) {
		//replace the word 'WORD' with you resource api endpoint
		//this function also has an id to get a specific resource
		return $http.get(urls.BASE_API + '/WORD/' + id)
			.success(success)
	}

	function put(data,success){
		return $http.put(urls.BASE_API + '/user' + data)
				.success(success)
	}

	function add_user(data, success, error){
		var auth = "Bearer " + window.sessionStorage.getItem("token").toString();
		var fd = new FormData();
		fd.append('name', data.name);
		if(data.email != null){
			fd.append('email', data.email);
		}
		if(data.password !=null) {
			fd.append('password', data.password);
		}

		fd.append('last_name', data.last_name);
		fd.append('first_name', data.first_name);
		if(data.registration_number != null){
			fd.append('registration_number', data.registration_number);
		}
		fd.append('role_id', data.role_id);
		fd.append('group_id', data.group_id);
		if(data.upload != null){
			fd.append('upload', data.upload);
		}
		$.ajax({
			url: urls.BASE_API + '/user',
			type: 'POST',
			data: fd,
			processData: false,
			contentType: false,
			"headers": {
				"authorization": auth
			},
			success: success,
			error: error
		});
	}

	function update_user(id,data, success, error){
		var auth = "Bearer " + window.sessionStorage.getItem("token").toString();
		var fd = new FormData();
		fd.append('name', data.name);
		if(data.email != null){
			fd.append('email', data.email);
		}
		if(data.password !=null) {
			fd.append('password', data.password);
		}

		fd.append('last_name', data.last_name);
		fd.append('first_name', data.first_name);
		if(data.registration_number != null){
			fd.append('registration_number', data.registration_number);
		}
		fd.append('role_id', data.role_id);
		fd.append('group_id', data.group_id);
		if(data.upload != null){
			fd.append('upload', data.upload);
		}

		if(data.use_gravatar !=null){
			fd.append('use_gravatar', data.use_gravatar);
		}
		fd.append('_method', 'PUT');
		$.ajax({
			url: urls.BASE_API + '/user/'+id,
			type: 'POST',
			data: fd,
			processData: false,
			contentType: false,
			"headers": {
				"authorization": auth
			},
			success: success,
			error: error
		});
	}
}