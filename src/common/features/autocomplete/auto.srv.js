angular
	.module('app.common.features.autocomplete')
	.factory('autoSrv', autoSrv);

autoSrv.$inject = [];

function autoSrv() {

	var service = {
		set_item: set_item,
		get_item: get_item
	}

	var item = null;

	return service;

	////////////////////////////

	function set_item(data) {

		item = data;
	}

	function get_item() {
		return item;
	}
}