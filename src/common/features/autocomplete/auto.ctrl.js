angular
    .module('app.common.features.autocomplete')
    .controller('auto', auto);

auto.$inject = [
    'usersSrv',
    'autoSrv',
    'urls'
]

function auto(usersSrv, autoSrv, urls) {

    var vm = this;
    vm.name = 'auto';

    vm.users = [];
    vm.querySearch = querySearch;
    vm.parent = null;
    vm.token = null;
    vm.img_url = null;

    activate();

    //////////////////////////////////////////

    function activate() {

        vm.img_url = urls.BASE_API;
        console.log('controller auto initialization');

        vm.token = window.sessionStorage.getItem("token");
        //vm.parent = $scope.$parent;
        //console.log(vm.parent);
        vm.parent = {
            item_select: null,
            item: null
        };
        init_autocomplete();
    }

    function init_autocomplete() {
        var crud = new usersSrv.crud();
        crud.get(function (d) {
            for (var i = 0; i < d.user.length; i++) {
                vm.users.push({
                    display: d.user[i],
                    value: d.user[i].name.toLowerCase(),
                    id: d.user[i].id
                });
            }
        })

    }

    function querySearch(query) {
        var results = query ? vm.users.filter(createFilterFor(query)) : vm.users;
        autoSrv.set_item(vm.parent);
        return results;
    }

    function createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(state) {
            return (state.value.indexOf(lowercaseQuery) === 0);
        };
    }
}