angular
    .module('app', [

        /*Shared modules*/
        'app.common',
        'app.routes',
        'app.layout',

        /*Feature areas*/
        //maybe if this will get bigger i will only include app.core and
        //in core folder i will have another module name core.mdl and put there all
        //the modules
        'app.core',
    ])
