angular
    .module("app.routes", ['ui.router', 'blocks.router'])
    .run(runBlock)
    .config(['$urlRouterProvider', '$httpProvider', function ($urlRouterProvider, $httpProvider) {


        $urlRouterProvider.otherwise('/page_not_found');

        $httpProvider.interceptors.push('httpRequestInterceptor');
    }]);

runBlock.$inject = [
    "$state",
    "$rootScope",
    "routerHelper"
];
function runBlock($state, $rootScope, routerHelper) {

    routerHelper.handleRoutingErrors();

    $rootScope.$on('$stateChangeError',function(event, toState, toParams, fromState, fromParams){
        console.log('$stateChangeError - fired when an error occurs during transition.');
        console.log(arguments);
        alert("Page not found");
    });


    $rootScope.$on("$stateChangeStart", function (event, to) {

        if (to.data && to.data.requireLogin) {

            if (!sessionStorage.getItem('token')) {
                event.preventDefault();
                $state.go("home");
            }
        }
    })
}
